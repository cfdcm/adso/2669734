# Condicional multicaso con: and, or, not
# Preguntar al usuario su nota final de algoritmos.
"""
Si su nota está entre 0 y 2, decir insuficiente.
Si está entre 2 y 3.4, perdió
Si está entre 3.5 y 4.5, ganó!!
Si es mayor de 4.5, Felicitaciones!!

Además si sacó 5.0 exactamente, generar una mención de honor. Que diga: "Usted ha ganado una Beca para la proxima carrera de $20 millones."

Adicionalmente si sacó 0 exactamente, indicar que ha sido expulsado de la Universidad."
"""

num = float(input("Digite su nota: "))

if num >=0 and num < 2:
    print("insuficiente")
elif num >=2 and num <= 3.4:
    print("perdió")
elif num >3.4 and num <= 4.5:
    print("ganó!")
elif num >4.5 and num <= 5:
    print("Felicitaciones!!")
else:
    print("Error")
    

if num == 0:
    print("Expulsado...")
elif num == 5:
    print("Beca")
