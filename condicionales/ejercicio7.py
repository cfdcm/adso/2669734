"""
0.0 Inaceptable
0.4 Aceptable
0.6 Meritorio   >

eur 2.400
"""

puntos = float(input("Puntuación del usuario: "))
dinero = 2400

if puntos == 0.0:
    print("Nivel: Inaceptable")
    dinero = dinero * puntos
elif puntos == 0.4:
    print("Nivel: Aceptable")
    dinero = dinero * puntos
elif puntos >= 0.6:
    print("Nivel: Meritorio")
    dinero = dinero * puntos
else:
    dinero = 0
print(f"El dinero según tu nivel es: €{dinero}")




