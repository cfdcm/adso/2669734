"""
Escribir un programa que pida al usuario dos números y muestre por pantalla
su división. Si el divisor es cero el programa debe mostrar un error.
"""

# numerador - dividendo
num1 = int(input("Digite un número entero:\n"))
# denominador - divisor
num2 = int(input("Digite otro número entero:\n"))

if num2 == 0:
    print("La división por cero no está definida")
else:
    div = num1/num2
    print(f"{num1}/{num2} = {div:.2f}")



