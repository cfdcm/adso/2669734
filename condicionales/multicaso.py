# Condicional multicaso...   if    elif       else
"""
if <condicion>:
    pass
elif <condicion>:
    pass
elif <condicion>:
    pass
elif <condicion>:
    pass
elif <condicion>:
    pass
else:
    pass
"""

# Averiguar el estado de un semáforo e indicar un mensaje según cada color.

estado = input("""
En que color está el semáforo?:
1.Rojo
2.Amarillo
3.Verde
: """).lower()

if estado == "rojo":
    print("Pare!!")
elif estado == "amarillo":
    print("Precaución!!")
elif estado == "verde":
    print("Siga...")
else:
    print("Error")


