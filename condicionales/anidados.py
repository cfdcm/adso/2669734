# Uso del MOD. %
# 1. Averiguar si un número N es par o no
# 2. Averiguar si un número N es impar o no
# 3. Averiguar si un número N es par o impar

n = int(input("Digite un número entero: "))


if n % 2 == 0:
    print("Es par")
else:
    print("No es par")


if n % 2 != 0:
    print("Es impar")
else:
    print("No es impar")

# 3 punto
if n % 2 != 0:
    print("Es impar")
else:
    print("Es par")
# ----------------------------------
if n % 2 == 0:
    print("Es par")
else:
    print("Es impar")
