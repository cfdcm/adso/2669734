# Multicaso

# Pedir un número entre 1 y 5 y convertirlo a palabra.
# Ejemplo: 3  -->  tres

num = int(input("Digite un número: "))
palabra = ""
if num == 1:
    palabra = "Uno"
elif num == 2:
    palabra = "Dos"
elif num == 3:
    palabra = "Tres"
elif num == 4:
    palabra = "Cuatro"
elif num == 5:
    palabra = "Cinco"
else:
    palabra = "Error: Fuera de rrando"
    
print(f"El num {num} corresponde a: {palabra}")
