"""
Los alumnos de un curso se han dividido en dos grupos A y B de acuerdo
al sexo y el nombre.

El grupo A esta formado por las mujeres con un nombre anterior a la M
y los hombres con un nombre posterior a la N y el grupo B por el resto.

Escribir un programa que pregunte al usuario su
nombre y sexo, y muestre por pantalla el grupo que le corresponde.
"""

#   A  -> mujeres anterior a M
#   B  -> hombre, posterior a N
#   resto -> hombre o mujeres, inician con M y N

#   A  -> mujeres anterior a M (inclusive)
#   B  -> hombre, posterior a N (inclusive)

genero = input("Digite su género: M, F, O: ").upper()
nombre = input("Digite su nombre: ").upper()

if genero == "M" and (nombre[0] == "O" or nombre[0] == "P" or nombre[0] == "Q" \
            or nombre[0] == "R" or nombre[0] == "S" or nombre[0] == "T" \
            or nombre[0] == "U" or nombre[0] == "V" or nombre[0] == "W" \
            or nombre[0] == "X" or nombre[0] == "Y" or nombre[0] == "Z"):
        print("Pertenece al grupo B")
elif genero == "F" and (nombre[0] == "A" or nombre[0] == "B" or nombre[0] == "C" \
            or nombre[0] == "D" or nombre[0] == "E" or nombre[0] == "F" \
            or nombre[0] == "G" or nombre[0] == "H" or nombre[0] == "I" \
            or nombre[0] == "J" or nombre[0] == "K" or nombre[0] == "L"):
        print("Pertenece al grupo A")
else:
    if genero == "O":
        if nombre[0] == "M" or nombre[0] == "N":
            print("Pertenece al resto tambíen por su letra inicial")
        else:
            print("Pertenece al resto, sólo por su género... la letra inicial no.")
    elif genero == "M" or genero == "F":
        print("Otro caso especial...")
    else:
        print("Error, género no identificado.")

