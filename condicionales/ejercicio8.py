"""
La pizzería Bella Napoli ofrece pizzas vegetarianas y no vegetarianas a sus clientes.
Los ingredientes para cada tipo de pizza aparecen a continuación.

    - Ingredientes vegetarianos: Pimiento y tofu.
    - Ingredientes no vegetarianos: Peperoni, Jamón y Salmón.

Escribir un programa que pregunte al usuario si quiere una pizza vegetariana o no,
y en función de su respuesta le muestre un menú con los ingredientes disponibles
para que elija. Solo se puede eligir un ingrediente además de la mozzarella y el
tomate que están en todas la pizzas. Al final se debe mostrar por pantalla si la
pizza elegida es vegetariana o no y todos los ingredientes que lleva.
"""

base1 = "Mozzarella"
base2 = "Tomate"

tipo = input("Quiere una pizza vegetariana? SI/NO: ").lower()

if tipo == "si":
    tipo = "vegetariana"
    seleccion = int(input("""Ingredientes vegetarianos:
        1. Pimientos
        2. Tofu
    """))
    if seleccion == 1:
        adicional = "Pimientos"
    elif seleccion == 2:
        adicional = "Tofu"
    else:
        adicional = ""

    if adicional == "":
        print("Error en la selección del adicional")
        print(f"Su pizza es {tipo} y sus ingredientes son {base1},{base2}")
    else:
        print(f"Su pizza es {tipo} y sus ingredientes son {base1},{base2} y {adicional}")
elif tipo == "no":
    tipo = "Carnívora"
    seleccion = int(input("""Ingredientes no vegetarianos:
            1. Peperoni
            2. Jamón
            3. Salmón
        """))
    if seleccion == 1:
        adicional = "Peperoni"
    elif seleccion == 2:
        adicional = "Jamón"
    elif seleccion == 3:
        adicional = "Salmón"
    else:
        adicional = ""

    if adicional == "":
        print("Error en la selección del adicional")
        print(f"Su pizza es {tipo} y sus ingredientes son {base1},{base2}")
    else:
        print(f"Su pizza es {tipo} y sus ingredientes son {base1},{base2} y {adicional}")
else:
    print("Error")
