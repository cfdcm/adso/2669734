# Condicionales. Número de 5 dígitos.

num = int(input("Digite un num de 5 caracteres 1-99999: "))

cifra1 = num % 10
print(cifra1)

aux = int((num - cifra1) / 10)
print(f"El num quedó así: {aux}")

cifra2 = aux % 10
print(cifra2)

aux = int((aux - cifra2) / 10)
print(f"El num quedó así: {aux}")



cifra3 = aux % 10
print(cifra3)

aux = int((aux - cifra3) / 10)
print(f"El num quedó así: {aux}")


cifra4 = aux % 10
print(cifra4)

aux = int((aux - cifra4) / 10)
print(f"El num quedó así: {aux}")


cifra5 = aux % 10
print(cifra5)

print(f"Num original {cifra5}{cifra4}{cifra3}{cifra2}{cifra1}")

cont_c1 = 0
if cifra1 == cifra2:
    cont_c1 += 1
if cifra1 == cifra3:
    cont_c1 += 1
if cifra1 == cifra4:
    cont_c1 += 1
if cifra1 == cifra5:
    cont_c1 += 1

cont_c2 = 0
if cifra2 == cifra1:
    cont_c2 += 1
if cifra2 == cifra3:
    cont_c2 += 1
if cifra2 == cifra4:
    cont_c2 += 1
if cifra2 == cifra5:
    cont_c2 += 1

cont_c3 = 0
if cifra3 == cifra1:
    cont_c3 += 1
if cifra3 == cifra2:
    cont_c3 += 1
if cifra3 == cifra4:
    cont_c3 += 1
if cifra3 == cifra5:
    cont_c3 += 1

cont_c4 = 0
if cifra4 == cifra1:
    cont_c4 += 1
if cifra4 == cifra2:
    cont_c4 += 1
if cifra4 == cifra3:
    cont_c4 += 1
if cifra4 == cifra5:
    cont_c4 += 1

cont_c5 = 0
if cifra5 == cifra1:
    cont_c5 += 1
if cifra5 == cifra2:
    cont_c5 += 1
if cifra5 == cifra3:
    cont_c5 += 1
if cifra5 == cifra4:
    cont_c5 += 1

print(f"Contador cifra1 {cifra1} es: {cont_c1}")
print(f"Contador cifra2 {cifra2} es: {cont_c2}")
print(f"Contador cifra3 {cifra3} es: {cont_c3}")
print(f"Contador cifra4 {cifra4} es: {cont_c4}")
print(f"Contador cifra5 {cifra5} es: {cont_c5}")


if cont_c1 > cont_c2 and cont_c1 > cont_c3 and cont_c1 > cont_c4 and cont_c1 > cont_c5:
    print(f"El número que más se repite es: {cifra1} ")
elif cont_c2 > cont_c1 and cont_c2 > cont_c3 and cont_c2 > cont_c4 and cont_c2 > cont_c5:
    print(f"El número que más se repite es: {cifra2} ")
elif cont_c3 > cont_c1 and cont_c3 > cont_c2 and cont_c3 > cont_c4 and cont_c3 > cont_c5:
    print(f"El número que más se repite es: {cifra3} ")
elif cont_c4 > cont_c1 and cont_c4 > cont_c2 and cont_c4 > cont_c3 and cont_c4 > cont_c5:
    print(f"El número que más se repite es: {cifra4} ")
elif cont_c5 > cont_c1 and cont_c5 > cont_c2 and cont_c5 > cont_c3 and cont_c5 > cont_c4:
    print(f"El número que más se repite es: {cifra5} ")
