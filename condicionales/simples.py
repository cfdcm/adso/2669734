# Condicionales Simples. De un caso.
"""
Operadores lógicos:
<       menor que
>       mayor que
<=      menor o igual que
>=      mayor o igual que
==      igual a
!=      diferente de 
and     y
or      o
not     negación
"""
hayChurrasco = "si"

# Identación, indentación, tabulación, sangría, formateo
if hayChurrasco == "si":
    print("Comer churrasco")
else:
    print("Continue buscando....")
    
    
# averiguar si una persona es mayor de edad o no.
# A partir de su edad. Pedida por pantalla.

edad = int(input("Digite su edad: "))

if edad <= 18:
    print("Menor de edad")
    
else:
    print("Es mayor de edad")




