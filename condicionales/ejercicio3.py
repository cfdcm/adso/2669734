"""Ejercicio 4
Solicitar un entero entre 1 y 50 y determinar sí es múltiplo de 8.
Recuerde el MOD.
    a. Si es múltiplo de 8, indique si es múltiplo de 5 también.
    b. Si no es múltiplo de 8, averigüe si es par.
"""

num = int(input("digite un num 1-50: "))

num = 52
#     V     and    F
#     A     and    B
if num >= 1 and num <= 50:
    if num % 8 == 0:
        print(f"El num: {num} es factor de 8")
        if num % 5 == 0:
            print(f"El num: {num} también es factor de 5")
        else:
            print(f"No es factor de 5")
    else:
        if num % 2 == 0:
            print("Es par")
        else:
            print("NO es par")

else:
    print("Número no válido...")


# Tabla de verdad: and

n1 = 80
n2 = 30
n3 = 5


if (n2 > 20 or n3 > 80) and (n2 > 20 or n3 > 80):
    print("Verdadero")
else:
    print("Falso")








