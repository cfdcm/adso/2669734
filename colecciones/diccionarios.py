# {} diccionarios           par clave:valor  ->   key:value

# estudiante = {}
estudiante = {
    "nombre": "Pedro",
    "apellido": "Martínez",
    "edad": 20,
    "deportes": [
        "F",
        [1, [1, 2, 3, {"algo": 20}], 3],
        "V"
    ],
    "direcciones": {
        "dir1": "medellín",
        "dir2": "estrella",
        "dir3": "bello"
    },
    "teléfonos": ()
}
estudiante["direcciones"]["dir3"]
estudiante["deportes"][1]
# Lectura o acceso
print(estudiante["edad"])

# Insertar o actualización
estudiante["correo"] = "pedro@sena.edu.co"
print(estudiante)
estudiante["apellido"] = "Pérez"
