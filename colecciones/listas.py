"""
# [] listas, arrays, vectores
frutas = []
frutas = ["manzana", "pera", "uva"]
# CRUD
# Lectura - Acceso
print(frutas[0])
print(frutas[1])
print(frutas[2])
# Actualizar

frutas[1] = ""
frutas[2] = "banano"

print(frutas)

# Borrado - Delete
# frutas.remove("banano")         # por valor
# print(frutas)
# frutas.pop(1)                   # por índice
# del frutas[0]                   # elimina por índice o toda la variable
# print(frutas)
# frutas.clear()                # limpia la lista


# Insertar o Create

frutas.append("maracuyá")       # Inserta al final
frutas.insert(7, "Mango")       # Inserta en posición específica
print(frutas)

# Otros     list()
# Slicing   : tajada, porción, acceso, lectura
print(frutas[-1])
print(frutas[0:3])      # 0,1,2
print(frutas[:3])       # 0,1,2
print(frutas[0:])       # 0,1,2
print(frutas[:])       # 0,1,2
"""
print("hola")
frutas = [-2, -8, -3, -9, -1, 2]
#print(frutas[-2:])   # -5,-4,-3,-2

frutas = [-2, -8, -3, -9, -1, 2]
print(frutas[::-1])
for indice, valor in enumerate(frutas):
    print(f"{indice} - Valor: {valor}")

