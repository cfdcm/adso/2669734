
# (, ) listas, arrays, vectores
frutas = ()
frutas = ("manzana", "pera", "uva")
# CRUD
# Lectura - Acceso
print(frutas[0])
print(frutas[1])
print(frutas[2])

# Otros     tuple()
# Slicing   : tajada, porción, acceso, lectura
print(frutas[-1])
print(frutas[0:3])      # 0,1,2
print(frutas[:3])       # 0,1,2
print(frutas[0:])       # 0,1,2
print(frutas[:])       # 0,1,2

print("hola")
frutas = [-2, -8, -3, -9, -1, 2]
#print(frutas[-2:])   # -5,-4,-3,-2

frutas = [-2, -8, -3, -9, -1, 2]
print(frutas[::-1])
for indice, valor in enumerate(frutas):
    print(f"{indice} - Valor: {valor}")

