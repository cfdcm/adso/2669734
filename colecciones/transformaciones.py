# Pasar de listas a tuplas y viceversa

frutas = ["manzana", "pera", "uva", "manzana"]
print(frutas)
frutas2 = tuple(frutas)
print(frutas2)
frutas3 = list(frutas2)
print(frutas3)

valores = range(5)
print(list(valores))
print(tuple(valores))

otro = tuple("Hola mundo...")
print(otro)
no_repetidos = set(frutas)
no_repetidos = list(no_repetidos)
print(no_repetidos)
