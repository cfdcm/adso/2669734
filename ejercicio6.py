# Construya un algoritmo que calcule la nota final de algoritmos, en el que cada parcial tiene un % distinto.

# Parcial1 = 30%
# Parcial2 = 25%
# Parcial3 = 45%

nota1 = float(input("Digite la nota del p1: "))
nota2 = float(input("Digite la nota del p2: "))
nota3 = float(input("Digite la nota del p3: "))

nota1 = nota1 * 0.30
nota2 = nota2 * 0.25
nota3 = nota3 * 0.45

print(f"La nota final es: {nota1 + nota2 + nota3:0.2f}")

