def calcular_pagar(h1, m1, h2, m2):
	mensaje_final = ""
	if h1 < h2:
		total_h = h2 - h1			# 4			3     1*60=60 -15
		total_m = m2 - m1			# -15
		if total_m < 0:
			total_h -= 1
			aux = 60 + total_m

		else:
			aux = total_m

		mensaje_final = f"Tiempo {total_h} y {aux} minutos\n"
		if aux > 0:
			total_h +=1

		mensaje_final += f"Total a pagar ${total_h*2000} por {total_h} horas"

	return mensaje_final

if __name__ == "__main__":
	h1 = int(input("Digite la hora1: "))
	m1 = int(input("Digite los min1: "))

	h2 = int(input("Digite la hora2: "))
	m2 = int(input("Digite los min2: "))

	print(calcular_pagar(h1, m1, h2, m2))
