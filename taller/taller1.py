def validar_correo(correo):
	cont_arrobas = 0
	cont_puntos = 0
	cont_antes_arroba = 0
	for l in correo:
		if l == "@":
			cont_arrobas += 1

		if l == ".":
			cont_puntos += 1
		if cont_arrobas == 0:
			cont_antes_arroba += 1
	# validación
	if cont_arrobas == 1 and cont_puntos >= 1 and cont_puntos <= 2 and cont_antes_arroba >=3:
		return True
	else:
		return False


if __name__ == "__main__":
	correo = input("Digite su correo: ")
	if validar_correo(correo):
		print("Correo electrónico Correcto!!")
	else:
		print("Correo erróneo...")