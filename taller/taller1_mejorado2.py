def validar_correo(correo):
	cont_arrobas = correo.count("@")
	cont_puntos = correo.count(".")
	# operador ternario
	cont_antes_arroba = len(correo[0:correo.find("@")]) if correo.find("@") > 0 else 0
	
	# validación
	if cont_arrobas == 1 and cont_puntos >= 1 and cont_puntos <= 2 and cont_antes_arroba >=3:
		return True
	else:
		return False


if __name__ == "__main__":
	correo = input("Digite su correo: ")		# jor@misena.edu.co
	if validar_correo(correo):
		print("Correo electrónico Correcto!!")
	else:
		print("Correo erróneo...")
