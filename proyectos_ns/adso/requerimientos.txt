# Librerías del Proyecto NativeScript

npm i nativescript-exit
npm i nativescript-modal-datetimepicker


<!-- Default style for DatePicker - in spinner mode -->
<style name="SpinnerDatePicker" parent="android:Widget.Material.Light.DatePicker">
    <!-- set the default mode for the date picker (supported values: spinner, calendar)  -->
    <item name="android:datePickerMode">calendar</item>
</style>

<!-- agregar la directiva de seguridad para consumir clear api's -->