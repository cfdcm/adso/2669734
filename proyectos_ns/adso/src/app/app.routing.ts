import { LoginComponent } from "./components/login/login";
import { HomeComponent } from "./components/home/home";
import { TiendaComponent } from "./components/tienda/tienda";
import { CategoriasComponent } from "./components/categorias/categorias";
import { CategoriasEditarComponent } from "./components/categorias/categorias-editar";
import { ProductosComponent } from "./components/productos/productos";
import { ProductosEditarComponent } from "./components/productos/productos-editar";
import { WidgetsComponent } from "./components/widgets/widgets";
import { Ejemplo1Component } from "./components/widgets/ejemplo1";
import { Ejemplo2Component } from "./components/widgets/ejemplo2";
import { Ejemplo3Component } from "./components/widgets/ejemplo3";

export const appRoutes: any = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "login", component: LoginComponent },
  { path: "home", component: HomeComponent},
  { path: "tienda", component: TiendaComponent },
  { path: "categorias", component: CategoriasComponent },
  { path: "categorias-editar", component: CategoriasEditarComponent },
  { path: "productos", component: ProductosComponent },
  { path: "productos-editar", component: ProductosEditarComponent },
  { path: "widgets", component: WidgetsComponent },
  { path: "ejemplo1", component: Ejemplo1Component },
  { path: "ejemplo2", component: Ejemplo2Component },
  { path: "ejemplo3", component: Ejemplo3Component },
];

export const appComponents: any = [
  LoginComponent,
  HomeComponent,
  TiendaComponent,
  CategoriasComponent,
  CategoriasEditarComponent,
  ProductosComponent,
  ProductosEditarComponent,
  WidgetsComponent,
  Ejemplo1Component,
  Ejemplo2Component,
  Ejemplo3Component,
];
