import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { Router } from "@angular/router";
import { exit } from "nativescript-exit";
import { SegmentedBar, SegmentedBarItem, SelectedIndexChangedEventData } from "@nativescript/core/ui/segmented-bar";


@Component({
  selector: 'ejemplo1',
  templateUrl: './ejemplo1.html'
})
export class Ejemplo1Component {
    ver_item0: boolean = true;
    ver_item1: boolean = false;
    ver_item2: boolean = false;
    ver_item3: boolean = false;

    otro_item0: boolean = true;
    otro_item1: boolean = false;
    otro_item2: boolean = false;

    mySegmentedBarItems: Array<SegmentedBarItem> = [];

    public constructor(private router: Router) {
    // Use the component constructor to inject providers.
        for (let i = 1; i < 5; i++) {
            const item = new SegmentedBarItem();
            item.title = "Item " + i;
            this.mySegmentedBarItems.push(item);
        }
    }

    public onSelectedIndexChange(args: SelectedIndexChangedEventData) {
        const segmentedBar = args.object as SegmentedBar;

        console.log('SegmentedBar index changed to:', segmentedBar.selectedIndex)
        this.ver_item0 = false;
        this.ver_item1 = false;
        this.ver_item2 = false;
        this.ver_item3 = false;
        if (segmentedBar.selectedIndex == 0){
            this.ver_item0 = true;
        }
        else if (segmentedBar.selectedIndex == 1){
            this.ver_item1 = true;
        }
        else if (segmentedBar.selectedIndex == 2){
            this.ver_item2 = true;
        }
        else if (segmentedBar.selectedIndex == 3){
            this.ver_item3 = true;
        }
        else{
            console.log("Ninguno")
        }

    }
    public onSelectedIndexChange2(args: SelectedIndexChangedEventData) {
        const segmentedBar = args.object as SegmentedBar;

        console.log('Categoría Index:', segmentedBar.selectedIndex)
        this.otro_item0 = false;
        this.otro_item1 = false;
        this.otro_item2 = false;
        switch(segmentedBar.selectedIndex){
            case 0:
                this.otro_item0 = true;
                break;
            case 1:
                this.otro_item1 = true;
                break;
            case 2:
                this.otro_item2 = true;
                break;
        }

    }
}
