import { Component, OnInit } from '@angular/core'
import { Router } from "@angular/router";
import { DatePicker } from "@nativescript/core/ui/date-picker";
import { ModalDatetimepicker, PickerOptions } from "nativescript-modal-datetimepicker";


@Component({
  selector: 'ejemplo2',
  templateUrl: './ejemplo2.html'
})
export class Ejemplo2Component {
    minDate: Date = new Date(1975, 0, 29);
    maxDate: Date = new Date(2045, 4, 12);
    public date: string = "2024-04-01";

    private modalDatetimepicker: ModalDatetimepicker;

    public constructor(private router: Router) {
        this.modalDatetimepicker = new ModalDatetimepicker();
    }

    public selectDate(){
        console.log("prueba---")
        this.modalDatetimepicker.pickDate(<PickerOptions>{
            title: "Configurable Title",
            theme: "light",
            startingDate: new Date(this.date),
            maxDate: new Date(),
            minDate: new Date('2020-01-01')
        }).then((result:any) => {
            if (result) {
                this.date = result.day + "-" + result.month + "-" + result.year;
            } else {
                this.date = ""; // false
            }
        })
        .catch((error) => {
            console.log("Error: " + error);
        });
    }


}
