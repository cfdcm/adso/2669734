import { Component, OnInit } from '@angular/core'
import { Router } from "@angular/router";

import { CopyTo, Create, Extensions, Modes, OpenFile } from "nativescript-filepickers";

const context = Create({
    extensions: ['pdf', 'xls', 'jpg', 'png'],
    mode: Modes.Single,
});

@Component({
  selector: 'ejemplo3',
  templateUrl: './ejemplo3.html'
})
export class Ejemplo3Component {
    selectedImages: any[];
    public constructor(private router: Router) {
        //....
    }

    public selectFile(){
        console.log("prueba---")
        context
            .Authorize()
            .then(() => {
            return context.Present();
            })
            .then((assets) => {
                assets.forEach((asset) => {
                    console.log("Real Path: " + asset);
                    try {
                        const newPath = CopyTo(asset, "./");
                        console.log("Copied Path: " + newPath);
                    } catch (error) {
                        console.error(error);
                        // Expected output: ReferenceError: nonExistentFunction is not defined
                        // (Note: the exact output may be browser-dependent)
                    }

                    //this.selectedImages.push(newPath);
                });
            });
    }


}
