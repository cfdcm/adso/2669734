import { Component, OnInit } from '@angular/core'
import { Router } from "@angular/router";

@Component({
  selector: 'tienda',
  templateUrl: './tienda.html',
})
export class TiendaComponent {

  public constructor(private router: Router) {
    // Use the component constructor to inject providers.
    console.log(localStorage.getItem('sena.token'));
    console.log(JSON.parse( localStorage.getItem('sena.user')) );
  }

  public onTap(){
    this.router.navigate(["home"]);
  }


}

