import { Component, OnInit } from '@angular/core'
import { Router } from "@angular/router";
import { ApiService } from './api.service';

import { Dialogs } from '@nativescript/core'
import { ActivatedRoute } from '@angular/router';
import { map, filter, scan } from 'rxjs/operators';
import { TextField } from "@nativescript/core/ui/text-field";


@Component({
    selector: 'categorias-editar',
    templateUrl: './categorias-editar.html',
})
export class CategoriasEditarComponent {
    id: number;
    nombre_cat: string;
    desc: string;
    boton: string;
    public constructor(private router: Router, private apiService: ApiService, private activatedRoute: ActivatedRoute ) {

        this.activatedRoute.queryParams
          .subscribe((params) => {
            if(params.id){
                this.boton = "Actualizar";
                this.id = params.id;
                this.apiService.getRegisterById(params.id).subscribe((res) => {
                    console.info(res)
                    this.nombre_cat = res.nombre_cat;
                    this.desc = res.desc;
                },error => {
                    console.log(error.status)
                    if (error.status == 400){
                        Dialogs.alert({
                            title: 'Respuesta:',
                            message: error.error.message,
                            okButtonText: 'OK',
                            cancelable: true,
                        });
                    }
                    else{
                        Dialogs.alert({
                            title: 'Respuesta:',
                            message: error.message,
                            okButtonText: 'OK',
                            cancelable: true,
                        });
                    }

                });

            }
            else{
                console.log("Nuevo....")
                this.boton = "Crear";
            }
          }
        );
    }

    public actualizarRegistro(){
        let data = {
            nombre_cat: this.nombre_cat,
            desc: this.desc
        };
        console.log(data)
        this.apiService.updateRegister(this.id, data).subscribe((res) => {
            console.info("ok")
            Dialogs.alert({
                title: 'Detalles!',
                message: 'Categoría actualizada correctamente!!',
                okButtonText: 'OK',
                cancelable: true,
            });
            this.router.navigate(['categorias']);
        });
    }

    inputChange(args, campo) {
        // blur event will be triggered when the user leaves the TextField
        let textField = <TextField>args.object;
        if (campo == "nombre_cat"){
            this.nombre_cat = textField.text;
        }
        else if(campo == "desc"){
            this.desc = textField.text;
        }
    }

    public guardarRegistro(){
        let data = {
            nombre_cat: this.nombre_cat,
            desc: this.desc
        };
        console.log(data)
        this.apiService.addRegister(data).subscribe((res) => {
            console.info("ok")
            Dialogs.alert({
                title: 'Detalles!',
                message: 'Categoría creada correctamente!!',
                okButtonText: 'OK',
                cancelable: true,
            });
            this.router.navigate(['categorias']);
        });
    }

    public operar(){
        if (this.boton == "Crear"){
            this.guardarRegistro();
        }
        else if(this.boton == "Actualizar"){
            this.actualizarRegistro();
        }
    }
}

