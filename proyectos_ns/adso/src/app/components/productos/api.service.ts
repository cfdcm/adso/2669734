import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getRegisters(): Observable<any[]> {
    return this.http.get<any[]>(`${apiUrl}/producto/`);
  }

  getRegisterById(id: number): Observable<any> {
    return this.http.get<any>(`${apiUrl}/producto/${id}/`);
  }

  addRegister(post: any): Observable<any> {
    return this.http.post<any>(`${apiUrl}/producto/`, post);
  }

  updateRegister(id: number, post: any): Observable<any> {
    return this.http.put<any>(`${apiUrl}/producto/${id}/`, post);
  }

  deleteRegister(id: number): Observable<any> {
    return this.http.delete<any>(`${apiUrl}/producto/${id}/`);
  }

  // Filtrar productos por categoría:
  getProductsByIdCat(id: number): Observable<any> {
    return this.http.get<any>(`${apiUrl}/producto_filtro_cat/${id}/`);
  }
}
