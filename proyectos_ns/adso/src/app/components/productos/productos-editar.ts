import { Component, OnInit } from '@angular/core'
import { Router } from "@angular/router";
import { ApiService } from './api.service';

import { Dialogs } from '@nativescript/core'
import { ActivatedRoute } from '@angular/router';
import { map, filter, scan } from 'rxjs/operators';
import { TextField } from "@nativescript/core/ui/text-field";


@Component({
    selector: 'productos-editar',
    templateUrl: './productos-editar.html',
})
export class ProductosEditarComponent {
    id: number;
    nombre: string;
    stock: string;
    precio: string;
    categorias: string;
    foto: string;

    boton: string;

    public constructor(private router: Router, private apiService: ApiService, private activatedRoute: ActivatedRoute ) {

        this.activatedRoute.queryParams
          .subscribe((params) => {
            if(params.id){
                this.boton = "Actualizar";
                this.id = params.id;
                this.apiService.getRegisterById(params.id).subscribe((res) => {
                    console.info(res)
                    this.nombre = res.nombre;
                    this.stock = res.stock;
                    this.precio = res.precio;
                    this.categorias = res.categorias;
                    //this.foto = res.foto;
                },error => {
                    console.log(error.status)
                    if (error.status == 400){
                        Dialogs.alert({
                            title: 'Respuesta:',
                            message: error.error.message,
                            okButtonText: 'OK',
                            cancelable: true,
                        });
                    }
                    else{
                        Dialogs.alert({
                            title: 'Respuesta:',
                            message: error.message,
                            okButtonText: 'OK',
                            cancelable: true,
                        });
                    }

                });

            }
            else{
                console.log("Nuevo....")
                this.boton = "Crear";
            }
          }
        );
    }

    public actualizarRegistro(){
        let data = {
            nombre : this.nombre,
            stock : this.stock,
            precio : this.precio,
            categorias : this.categorias/*,
            foto : this.foto*/
        };
        console.log(data)
        this.apiService.updateRegister(this.id, data).subscribe((res) => {
            console.info("ok")
            Dialogs.alert({
                title: 'Detalles!',
                message: 'Producto actualizado correctamente!!',
                okButtonText: 'OK',
                cancelable: true,
            });
            this.router.navigate(['productos']);
        },error => {
            console.log(error.status)
            if (error.status == 400){
                Dialogs.alert({
                    title: 'Respuesta:',
                    message: error.error.message,
                    okButtonText: 'OK',
                    cancelable: true,
                });
            }
            else{
                Dialogs.alert({
                    title: 'Respuesta:',
                    message: error.message,
                    okButtonText: 'OK',
                    cancelable: true,
                });
            }

        });
    }

    inputChange(args, campo) {
        // blur event will be triggered when the user leaves the TextField
        let textField = <TextField>args.object;
        if (campo == "nombre"){
            this.nombre = textField.text;
        }
        else if(campo == "stock"){
            this.stock = textField.text;
        }
        else if(campo == "precio"){
            this.precio = textField.text;
        }
        else if(campo == "categorias"){
            this.categorias = textField.text;
        }
        /*else if(campo == "foto"){
            this.foto = textField.text;
        }*/
    }

    public guardarRegistro(){
        let data = {
            nombre : this.nombre,
            stock : this.stock,
            precio : this.precio,
            categorias : this.categorias/*,
            foto : this.foto*/
        };
        console.log(data)
        this.apiService.addRegister(data).subscribe((res) => {
            console.info("ok")
            Dialogs.alert({
                title: 'Detalles!',
                message: 'Producto creado correctamente!!',
                okButtonText: 'OK',
                cancelable: true,
            });
            this.router.navigate(['productos']);
        });
    }

    public operar(){
        if (this.boton == "Crear"){
            this.guardarRegistro();
        }
        else if(this.boton == "Actualizar"){
            this.actualizarRegistro();
        }
    }
}

