import { Component, OnInit } from '@angular/core'
import { Router } from "@angular/router";
import { ApiService } from './api.service';
import { ItemEventData } from "@nativescript/core/ui/list-view";
import { Dialogs } from '@nativescript/core'
// Lo requiero para los filtros
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'productos',
    templateUrl: './productos.html',
})
export class ProductosComponent {
    productos: any[];
    mensaje: string = "";
    public constructor(private router: Router, private apiService: ApiService, private activatedRoute: ActivatedRoute) {
        // Use the component constructor to inject providers.
        this.activatedRoute.queryParams
          .subscribe((params) => {
            if(params.id){
                // filtro
                console.log(`Cat para filtro: ${params.id}`)
                this.filtroPorCat(params.id);
            }
            else{
                this.obtenerTodos();
            }
        });
    }

    public obtenerTodos(){
        this.apiService.getRegisters().subscribe((data: any[]) => {
            //console.log(data);
            this.productos = data;
        });
    }

    onItemTap(args: ItemEventData) {
        let register = this.productos[args.index]
        //console.log(`Index: ${args.index}; Item: ${register.id}`);
        //console.log(`ID: ${register.id} - NOMBRE: ${register.nombre_cat} - DESCRCIPCIÓN: ${register.desc} `)

        //Consultar por ID en la API
        this.apiService.getRegisterById(register.id).subscribe((res) => {
            Dialogs.alert({
                title: 'Detalles!',
                message: `ID: ${res.id}\nNOMBRE: ${res.nombre}\nSTOCK: ${res.stock}\nPRECIO: ${res.precio}\nCATEGORÍA: ${res.categoria_name} `,
                okButtonText: 'OK',
                cancelable: true,
            });
            console.info(res)
        });
    }

    public eliminar(item){
        Dialogs.confirm({
            title: 'Confirmación',
            message: 'Está seguro de eliminar este registro ?',
            okButtonText: 'SI',
            cancelButtonText: 'No',
            neutralButtonText: 'Cancelar',
            })
            .then((result) => {
                console.log(result);
                if (result){
                    this.apiService.deleteRegister(item.id).subscribe((res: string) => {
                        Dialogs.alert({
                            title: 'Respuesta:',
                            message: "Producto eliminado correctamente!!",
                            okButtonText: 'OK',
                            cancelable: true,
                        });
                        this.obtenerTodos();
                    },error => {
                        console.log(error.status)
                        if (error.status == 400){
                            Dialogs.alert({
                                title: 'Respuesta:',
                                message: error.error.message,
                                okButtonText: 'OK',
                                cancelable: true,
                            });
                        }
                        else{
                            Dialogs.alert({
                                title: 'Respuesta:',
                                message: error.message,
                                okButtonText: 'OK',
                                cancelable: true,
                            });
                        }

                    });
                }
            });

    }

    public editar(item){
        console.log(`Editar prod: ${item.id}`)
        this.router.navigate(['productos-editar'], { queryParams: { id: item.id } });
    }

    public agregar(){
        this.router.navigate(['productos-editar']);
    }

    public filtroPorCat(id){
        this.apiService.getProductsByIdCat(id).subscribe((data: any[]) => {
            console.log(data);
            if (data.length == 0){
                this.mensaje = "No hay productos en esta cateogría....";
                Dialogs.alert({
                    title: 'Respuesta:',
                    message: "No hay productos en esta cateogría...",
                    okButtonText: 'OK',
                    cancelable: true,
                });
            }
            else{
                this.mensaje = "";
            }
            this.productos = data;
        });
    }
}

