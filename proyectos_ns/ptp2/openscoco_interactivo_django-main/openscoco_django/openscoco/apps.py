from django.apps import AppConfig


class OpenscocoConfig(AppConfig):
    name = 'openscoco'
