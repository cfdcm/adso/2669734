Ext.onReady(function(){
    $('#spinner').hide();
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : [
                    {
                        id: 'l6',
                        text: 'print(f"La cantidad de pares encontrados es:  {cont} ")',
                        height: 50,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l3',
                        text: 'if num % 2 == 0:',
                        height: 50,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l1',
                        text: 'cont = 0',
                        height: 50,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l5',
                        text: 'cont += 1',
                        height: 50,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l2',
                        text: 'for num in range(34, 87):',
                        height: 50,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l4',
                        text: 'print(f"El número {num} es par")',
                        height: 50,
                        width: '90%',
                        margin: 2
                    }
                ]
            });
        }
    });
});





