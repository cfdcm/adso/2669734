function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Ext.onReady(function(){
    $('#spinner').hide();
    var datos = [
        {
            id: 'l0',
            text: 'tarea=[]',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l1',
            text: 'for i in range(10):',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l2',
            text: 'do=True',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l3',
            text: 'num = 0',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l4',
            text: 'while do or (num<1 or num >3):',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l5',
            text: 'do = False',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l6',
            text: 'num = int(input("Por favor digite un num entre 1-3: "))',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l7',
            text: 'tarea.append(num)',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l8',
            text: 'print(tarea)',
            height: 25,
            width: '90%',
            margin: 2
        }
        
    ]
    //console.log(datos);
    var randomDatos = shuffle(datos);
    //console.log(randomDatos);
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : randomDatos
            });
        }
    });

});