Ext.onReady(function(){
    $('#spinner').hide();
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : [
                    {
                        id: 'l12',
                        text: 'intentos += 1',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l10',
                        text: 'break',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l8',
                        text: 'if usuario == "adsi" and clave == "OC2021*1":',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l7',
                        text: 'clave = input("Digite su contraseña: ")',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l9',
                        text: 'logueado = True',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l14',
                        text: 'print(f"Bienvenido:  {usuario} ")',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l6',
                        text: 'usuario = input("Digite su nombre de usuario: ")',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l15',
                        text: 'else:					# no pertenece al while',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l5',
                        text: 'do = False',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l1',
                        text: 'intentos = 0',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l2',
                        text: 'logueado = False',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l0',
                        text: 'usuario = clave = ""',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l4',
                        text: 'while do or ( not logueado and intentos < 3 ):',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l13',
                        text: 'if logueado:',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l3',
                        text: 'do = True',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l11',
                        text: 'else: 				# pertenece al while',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l16',
                        text: 'print(f"Máximo de intentos superados. Usuario o Contraseña incorrectos.")',
                        height: 30,
                        width: '90%',
                        margin: 2
                    }
                ]
            });
        }
    });
});





