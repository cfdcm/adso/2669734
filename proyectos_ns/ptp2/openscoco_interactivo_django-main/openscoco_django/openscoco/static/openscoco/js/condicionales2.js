function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Ext.onReady(function(){
    $('#spinner').hide();
    var datos = [
        {
            id: 'l0',
            text: 'e1Esp = float(input("Digite la nota Español E1"));',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l1',
            text: 'e2Esp = float(input("Digite la nota Español E2"));',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l2',
            text: 'e1Mat = float(input("Digite nota Matemát E1"));',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l3',
            text: 'e2Mat = float(input("Digite nota Matemát E2"));',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l4',
            text: 'if e1Esp > e2Esp:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l5',
            text: 'print("Estudiante 1 Gana Beca!")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l6',
            text: 'elif e1Esp < e2Esp:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l7',
            text: 'print("Estudiante 2 Gana Beca!")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l8',
            text: 'elif e1Esp == e2Esp:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l9',
            text: 'if e1Mat > e2Mat:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l10',
            text: 'print("Desempate, Estudiante 1 Gana Beca!")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l11',
            text: 'elif e1Mat < e2Mat:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l12',
            text: 'print("Desempate, Estudiante 2 Gana Beca!")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l13',
            text: 'else:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l14',
            text: 'print("Ambos Estudiantes ganan Beca!")',
            height: 25,
            width: '90%',
            margin: 2
        }
    ]
    //console.log(datos);
    var randomDatos = shuffle(datos);
    //console.log(randomDatos);
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : randomDatos
            });
        }
    });

});





