function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Ext.onReady(function(){
    $('#spinner').hide();
    var datos = [
        {
            id: 'l0',
            text: 'from random import randint',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l1',
            text: 'baloto = []',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l2',
            text: 'for x in range(5):	#primer ciclo',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l3',
            text: 'baloto.append(randint(1, 45))',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l4',
            text: 'usuario = []',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l5',
            text: 'for n in range(5):	#segundo ciclo',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l6',
            text: 'usuario.append(int(input("Digite un número entre 1-45: ")))',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l7',
            text: 'aciertos = []',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l8',
            text: 'for b in baloto:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l9',
            text: 'for u in usuario:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l10',
            text: 'if b == u:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l11',
            text: 'aciertos.append(b)',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l12',
            text: 'if len(aciertos) >= 3:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l13',
            text: 'print("Ganador!!")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l14',
            text: 'else:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l15',
            text: 'print("Siga participando!!")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l16',
            text: 'print(f"Aciertos {aciertos}")',
            height: 25,
            width: '90%',
            margin: 2
        }
    ]
    //console.log(datos);
    var randomDatos = shuffle(datos);
    //console.log(randomDatos);
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : randomDatos
            });
        }
    });

});





