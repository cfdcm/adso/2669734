Ext.onReady(function(){
    $('#spinner').hide();
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : [
                    {
                        id: 'l1',
                        text: 'suma = 0',
                        height: 50,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l3',
                        text: 'for i in edades:',
                        height: 50,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l0',
                        text: 'edades = [20, 35, 18, 83, 50, 65, 20, 18]',
                        height: 50,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l2',
                        text: 'promedio = 0',
                        height: 50,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l4',
                        text: 'suma += i',
                        height: 50,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l6',
                        text: 'print(f"El promedio de las edades es:  {promedio} ")',
                        height: 50,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l5',
                        text: 'promedio = suma / len(edades)',
                        height: 50,
                        width: '90%',
                        margin: 2
                    }
                ]
            });
        }
    });
});





