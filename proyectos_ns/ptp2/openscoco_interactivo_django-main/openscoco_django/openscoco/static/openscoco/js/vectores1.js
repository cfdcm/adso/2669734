function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Ext.onReady(function(){
    $('#spinner').hide();
    var datos = [
        {
            id: 'l0',
            text: 'estudiantes = []',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l1',
            text: 'for x in range(3):	#primer ciclo',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l2',
            text: 'datos = []',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l3',
            text: 'for y in range(2):	#segundo ciclo',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l4',
            text: 'nota = float(input(f"Digite la nota final del curso: "))',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l5',
            text: 'datos.append(nota)',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l6',
            text: 'estudiantes.append(datos)',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l7',
            text: 'mejor_estudiante = -1	#ubique en el medio',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l8',
            text: 'promedio_mayor = 0		#ubique en el medio',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l9',
            text: 'for x in range(3):	#tercer ciclo',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l10',
            text: 'suma = 0',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l11',
            text: 'for y in range(2):	#cuarto ciclo',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l12',
            text: 'suma += estudiantes[x][y]',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l13',
            text: 'if promedio_mayor < (suma/2):',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l14',
            text: 'promedio_mayor = (suma/2)',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l15',
            text: 'mejor_estudiante = x',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l16',
            text: 'print(f"Estudiante {mejor_estudiante+1} es el mejor")',
            height: 25,
            width: '90%',
            margin: 2
        }
    ]
    //console.log(datos);
    var randomDatos = shuffle(datos);
    //console.log(randomDatos);
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : randomDatos
            });
        }
    });

});