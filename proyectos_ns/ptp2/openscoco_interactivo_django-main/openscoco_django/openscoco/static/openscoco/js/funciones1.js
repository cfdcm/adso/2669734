function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Ext.onReady(function(){
    $('#spinner').hide();
    var datos = [
        {
            id: 'l0',
            text: 'def negritas(texto):',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l1',
            text: 'return "***," + texto',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l2',
            text: 'def cursiva(texto):',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l3',
            text: 'return "---," + texto',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l4',
            text: 'def subrayado(texto):',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l5',
            text: 'return "sub," + texto',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l6',
            text: 'if __name__ == "__main__":',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l7',
            text: 'print( negritas("ADSI") )',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l8',
            text: 'print( cursiva("ADSI") )',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l9',
            text: 'print( subrayado("ADSI") )',
            height: 35,
            width: '90%',
            margin: 2
        }
    ]
    //console.log(datos);
    var randomDatos = shuffle(datos);
    //console.log(randomDatos);
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : randomDatos
            });
        }
    });

});