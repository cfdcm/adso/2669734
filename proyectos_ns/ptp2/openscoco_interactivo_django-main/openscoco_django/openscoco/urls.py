from django.urls import path
from . import views

app_name = 'openscoco'

urlpatterns = [
    path('', views.index, name='index'),
    path('entrenar/', views.entrenar, name='entrenar'),
    path('reto1', views.reto1, name='reto1'),
    path('reto2', views.reto2, name='reto2'),
    path('reto3', views.reto3, name='reto3'),
    path('reto4', views.reto4, name='reto4'),
    path('reto5', views.reto5, name='reto5'),
    path('reset', views.reset, name='reset'),

    path('condicionales1', views.condicionales_1, name='condicionales1'),
    path('condicionales2', views.condicionales_2, name='condicionales2'),
    path('condicionales3', views.condicionales_3, name='condicionales3'),
    path('condicionales4', views.condicionales_4, name='condicionales4'),
    path('condicionales5', views.condicionales_5, name='condicionales5'),
    path('reset2', views.reset2, name='reset2'),
    
    path('ciclos1', views.ciclos_1, name='ciclos1'),
    path('ciclos2', views.ciclos_2, name='ciclos2'),
    path('ciclos3', views.ciclos_3, name='ciclos3'),
    path('ciclos4', views.ciclos_4, name='ciclos4'),
    path('ciclos5', views.ciclos_5, name='ciclos5'),
    path('reset3', views.reset3, name='reset3'),
    
    path('funciones1', views.funciones_1, name='funciones1'),
    path('funciones2', views.funciones_2, name='funciones2'),
    path('funciones3', views.funciones_3, name='funciones3'),
    path('funciones4', views.funciones_4, name='funciones4'),
    path('funciones5', views.funciones_5, name='funciones5'),
    path('reset4', views.reset4, name='reset4'),
    
    path('vectores1', views.vectores_1, name='vectores1'),
    path('vectores2', views.vectores_2, name='vectores2'),
    path('vectores3', views.vectores_3, name='vectores3'),
    path('vectores4', views.vectores_4, name='vectores4'),
    path('vectores5', views.vectores_5, name='vectores5'),
    path('reset5', views.reset5, name='reset5'),
    
]
