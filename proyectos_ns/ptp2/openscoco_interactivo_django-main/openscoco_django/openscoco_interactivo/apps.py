from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'openscoco_interactivo'
