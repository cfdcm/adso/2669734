$('#element').toast('show')
function reto1(ruta){
    valor = $( "#respuesta" ).val()
    if (valor.toLowerCase() == "true,false" ||  valor.toLowerCase() == "false,true"){        
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
}

function reto2(ruta){
    valor = $( "#respuesta" ).val()
    if (valor.toLowerCase() == "casting tipo entero" ||  valor.toLowerCase() == "casting tipo int"){
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
}

function reto3(ruta){
    valor = $( "#respuesta" ).val()
    if (valor == "print(f'El resultado de la suma de {A} + {B} = {SUMA}')" ||  valor == "print(f'El resultado de la suma de {A}+{B}={SUMA}')" || valor == "print('El resultado de la suma de', A, '+', B, ' = ', SUMA)"){
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
}

function reto4(ruta){
    valor = $( "#respuesta" ).val()
    if (valor == "%" || valor.toLowerCase() == "porcentaje" || valor.toLowerCase() == "tanto por ciento"){
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
}




function condicionales1(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8"){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}

function condicionales2(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8" && opciones.items.keys[9] == "l9" && opciones.items.keys[10] == "l10" && opciones.items.keys[11] == "l11" && opciones.items.keys[12] == "l12" && opciones.items.keys[13] == "l13" && opciones.items.keys[14] == "l14" ){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}

function condicionales3(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8" && opciones.items.keys[9] == "l9" && opciones.items.keys[10] == "l10" ){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}


function condicionales4(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8" && opciones.items.keys[9] == "l9" && opciones.items.keys[10] == "l10" && opciones.items.keys[11] == "l11" && opciones.items.keys[12] == "l12" && opciones.items.keys[13] == "l13" && opciones.items.keys[14] == "l14" && opciones.items.keys[15] == "l15" && opciones.items.keys[16] == "l16" && opciones.items.keys[17] == "l17" && opciones.items.keys[18] == "l18" && opciones.items.keys[19] == "l19" && opciones.items.keys[20] == "l20" && opciones.items.keys[21] == "l21"){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}


function ciclos1(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    /*for(i=0; i<opciones.items.length; i++){
        console.log(opciones.items.keys[i]);
    }*/
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l1" && opciones.items.keys[1] == "l2" && opciones.items.keys[2] == "l3" && ((opciones.items.keys[3] == "l4" && opciones.items.keys[4] == "l5") || (opciones.items.keys[3] == "l5" && opciones.items.keys[4] == "l4")) && opciones.items.keys[5] == "l6"){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}

function ciclos2(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8" && opciones.items.keys[9] == "l9" && opciones.items.keys[10] == "l10" && opciones.items.keys[11] == "l11" && opciones.items.keys[12] == "l12" && opciones.items.keys[13] == "l13" && opciones.items.keys[14] == "l14" && opciones.items.keys[15] == "l15" && opciones.items.keys[16] == "l16"){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}

function ciclos3(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(((opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2") || (opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l2" && opciones.items.keys[2] == "l1") || (opciones.items.keys[0] == "l1" && opciones.items.keys[1] == "l0" && opciones.items.keys[2] == "l2") || (opciones.items.keys[0] == "l1" && opciones.items.keys[1] == "l2" && opciones.items.keys[2] == "l0") || (opciones.items.keys[0] == "l2" && opciones.items.keys[1] == "l0" && opciones.items.keys[2] == "l1") || (opciones.items.keys[0] == "l2" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l0")) && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6"){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}


function ciclos4(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(((opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2") || (opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l2" && opciones.items.keys[2] == "l1") || (opciones.items.keys[0] == "l1" && opciones.items.keys[1] == "l0" && opciones.items.keys[2] == "l2") || (opciones.items.keys[0] == "l1" && opciones.items.keys[1] == "l2" && opciones.items.keys[2] == "l0") || (opciones.items.keys[0] == "l2" && opciones.items.keys[1] == "l0" && opciones.items.keys[2] == "l1") || (opciones.items.keys[0] == "l2" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l0")) && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && ((opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8") || (opciones.items.keys[7] == "l8" && opciones.items.keys[8] == "l7")) && opciones.items.keys[9] == "l9"){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}



function funciones1(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8" && opciones.items.keys[9] == "l9"){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}

function funciones2(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" ){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}

function funciones3(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8" && opciones.items.keys[9] == "l9" && opciones.items.keys[10] == "l10" && opciones.items.keys[11] == "l11" && opciones.items.keys[12] == "l12" ){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}


function funciones4(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8" && opciones.items.keys[9] == "l9" && opciones.items.keys[10] == "l10" && opciones.items.keys[11] == "l11" && opciones.items.keys[12] == "l12" && opciones.items.keys[13] == "l13" && opciones.items.keys[14] == "l14" && opciones.items.keys[15] == "l15" && opciones.items.keys[16] == "l16" && opciones.items.keys[17] == "l17"){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}


function vectores1(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8" && opciones.items.keys[9] == "l9" && opciones.items.keys[10] == "l10" && opciones.items.keys[11] == "l11" && opciones.items.keys[12] == "l12" && opciones.items.keys[13] == "l13" && opciones.items.keys[14] == "l14" && opciones.items.keys[15] == "l15" && opciones.items.keys[16] == "l16"){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}

function vectores2(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8" && opciones.items.keys[9] == "l9" && opciones.items.keys[10] == "l10" && opciones.items.keys[11] == "l11" && opciones.items.keys[12] == "l12" && opciones.items.keys[13] == "l13" && opciones.items.keys[14] == "l14" && opciones.items.keys[15] == "l15" && opciones.items.keys[16] == "l16" ){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}

function vectores3(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8" && opciones.items.keys[9] == "l9" && opciones.items.keys[10] == "l10" ){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}


function vectores4(ruta){
    //obtener la primera toolbar
    var opciones = Ext.ComponentQuery.query('#toolbar')[0];
    
    //averiguo si los items de la toolbar reordenable están en orden o no. Es importante que el la creación se le asigne el ID correcto según el orden real.
    if(opciones.items.keys[0] == "l0" && opciones.items.keys[1] == "l1" && opciones.items.keys[2] == "l2" && opciones.items.keys[3] == "l3" && opciones.items.keys[4] == "l4" && opciones.items.keys[5] == "l5" && opciones.items.keys[6] == "l6" && opciones.items.keys[7] == "l7" && opciones.items.keys[8] == "l8" ){
        //console.log("Correcto");
        $( "#rmodal" ).html("Respuesta Correcta!");
        $("#rboton").attr("href", ruta);
        $("#rboton").attr("data-dismiss", "");
        $('#exampleModalCenter').modal('toggle');
    }
    else{
        //console.log("Incorrecto");
        $( "#rmodal" ).html("Respuesta incorrecta.<br><br>Intente de Nuevo");
        $("#rboton").attr("href", "#");
        $("#rboton").attr("data-dismiss", "modal");
        $('#exampleModalCenter').modal('toggle');
    }
    
}

function reset(ruta){
    $( "#rmodal" ).html("Está seguro?");
    $("#rboton").attr("href", ruta);
    $("#rboton").attr("data-dismiss", "");
    $('#exampleModalCenter').modal('toggle');
}

function recargar(ruta){
    //location.href=ruta;
    window.location.reload();
}