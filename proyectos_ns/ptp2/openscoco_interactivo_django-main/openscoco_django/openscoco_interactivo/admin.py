from django.contrib import admin

# Aplicar Pygments al código
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.lexers import guess_lexer
from pygments.formatters import HtmlFormatter

# Register your models here.
from .models import *

from rest_framework.authtoken.admin import TokenAdmin

TokenAdmin.raw_id_fields = ['user']


@admin.register(Tema)
class TemaAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'descripcion', 'usuario_creador')


@admin.register(Problema)
class ProblemaAdmin(admin.ModelAdmin):
    list_display = ('id', 'tema', 'usuario_creador', 'titulo', 'descripcion', 'fecha', 'moderado', 'nivel')


@admin.register(Solucion)
class SolucionAdmin(admin.ModelAdmin):
    list_display = ('id', 'problema', 'usuario_creador', 'pretty', 'lenguaje_prog', 'moderado', 'fecha')
    list_editable = ['moderado']
    def pretty(self, obj):
        # Aplicar Pygments al código
        try:
            lexer = get_lexer_by_name(obj.lenguaje_prog, stripall=True)
        except Exception as e:
            lexer = guess_lexer(obj.descripcion, stripall=True)

        formatter = HtmlFormatter(linenos=True, noclasses="True", style='default')  # paraiso-dark
        result = '<div style="width: 500px; overflow-y: auto;">'+highlight(obj.descripcion, lexer, formatter)+'</div>'

        from django.utils.html import mark_safe

        return mark_safe(result)


@admin.register(Comentario)
class ComentarioAdmin(admin.ModelAdmin):
    list_display = ('id', 'solucion', 'usuario_creador', 'mensaje', 'calificacion', 'moderado', 'fecha')


