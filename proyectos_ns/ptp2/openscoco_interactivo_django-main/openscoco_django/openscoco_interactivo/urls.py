from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views

from .views import *

router = DefaultRouter()
# router = DefaultRouter(trailing_slash=False)      # Para desactivar el slash final, si se requiere

router.register(r'users', UserViewSet)
router.register(r"temas", TemaViewSet)
router.register(r"problemas", ProblemaViewSet)
router.register(r"soluciones", SolucionViewSet)
router.register(r"comentarios", ComentarioViewSet)


app_name = 'openscoco_interactivo'

urlpatterns = [
    path('', index, name='index'),
    path("api/1.0/", include(router.urls)),
    path('api/1.0/api_generate_token/', views.obtain_auth_token),
    path('api/1.0/obtain_user_data/', CustomAuthToken.as_view()),
    path('api/1.0/problemas/tema/<int:tema>/', ProblemaList.as_view()),
    path('api/1.0/soluciones/problema/<int:problema>/', SolucionList.as_view()),
    path('api/1.0/comentarios/solucion/<int:solucion>/', ComentarioList.as_view()),
]
