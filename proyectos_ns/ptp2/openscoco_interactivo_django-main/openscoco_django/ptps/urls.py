from django.urls import path
from . import views

app_name = 'ptps'

urlpatterns = [
    path('', views.index, name='index'),
]
