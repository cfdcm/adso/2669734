from django.shortcuts import render

# Create your views here.
def index(request):
    # Sitio web de mis productos técnico pedagógicos. Apps OpensCoco y OpensCoco iNteractivo
    return render(request, 'ptps/index.html')