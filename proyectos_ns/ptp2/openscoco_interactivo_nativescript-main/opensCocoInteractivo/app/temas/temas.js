var dialogModule = require('@nativescript/core/ui/dialogs');
var observableModule = require('@nativescript/core/data/observable');
var ObservableArray = require('@nativescript/core/data/observable-array').ObservableArray;
var TemaListViewModel = require('../shared/view-models/tema-list-view-model');
var frameModule = require('@nativescript/core/ui/frame');
var config = require("../shared/config");

var page;


var temaList = new TemaListViewModel([]);

var pageData = new observableModule.fromObject({
    temaList: temaList,
    nombre: '',
    descripcion: '',
    usuario_creador: '',
    puedeVer: "",
    puedeVer2: ""
});

exports.regresar = function (args) {
  console.log("Regresar al login...")
  frameModule.Frame.topmost().navigate({ moduleName: "login/login" });
};

exports.loaded = function (args) {
    page = args.object;

    var listView = page.getViewById('temaList');
    page.bindingContext = pageData;
    //console.log("Datos" + JSON.stringify(page.bindingContext))
    temaList.empty();
    pageData.set('isLoading', true);
    temaList.load().then(function () {
        pageData.set('isLoading', false);
        listView.animate({
            opacity: 1,
            duration: 1000
        });
    });

    pageData.set('user', "Bienvenido ("+config.rol+"), "+config.nombre);
    if (config.rol == "Aprendiz"){
      pageData.set('puedeVer', 'collapsed');
    }
    else{
      pageData.set('puedeVer', 'visible');
    }
    console.log(" =========== TEMAS Page =============")
};

exports.delete = function (args) {
    var item = args.view.bindingContext;
    var index = temaList.indexOf(item);

    dialogModule.confirm({
      title: "Confirmación ??",
      message: "Está seguro de eliminar este tema? " + temaList.getItem(index).nombre,
      okButtonText: "Aceptar",
      cancelButtonText: "Cancelar",
    }).then(function (result) {
      if(result){
        //eliminar
        temaList.delete(index);
      }
    });
}

exports.onItemTap = function(args) {
  var item = args.view.bindingContext;
  var index = temaList.indexOf(item);
  console.log(temaList.getItem(index).id);

  config.id_tema = temaList.getItem(index).id;
  config.id_tema_nombre = temaList.getItem(index).nombre;

  const navigationEntry = {
    moduleName: "problemas/problemas",
    context: {
      tema_id: temaList.getItem(index).id
    }
  };

  frameModule.Frame.topmost().navigate(navigationEntry);

}

exports.add_temas_page = function() {
  frameModule.Frame.topmost().navigate({moduleName: "temas/add-temas"});
}
