var dialogModule = require('@nativescript/core/ui/dialogs');
var observableModule = require('@nativescript/core/data/observable');
var TemaListViewModel = require('../shared/view-models/tema-list-view-model');
var frameModule = require('@nativescript/core/ui/frame');
var config = require("../shared/config");

var page;
var temaList = new TemaListViewModel([]);
var pageData = new observableModule.fromObject({
  temaList: temaList,
  nombre: '',
  descripcion: '',
  usuario_creador: config.user_id,
  puedeVer: ""
});

exports.regresar = function (args) {
  console.log("Regresar a TEMAS...")
  frameModule.Frame.topmost().navigate({ moduleName: "temas/temas" });
};

exports.loaded = function (args) {
  page = args.object;
  page.bindingContext = pageData;
  pageData.set('user', "Bienvenido ("+config.rol+"), "+config.nombre);
  pageData.set('usuario_creador', config.user_id);
  console.log(" =========== ADD TEMAS Page =============")
};

exports.add = function () {
  // Check for empty submission
  if (pageData.get('nombre').trim() === '' || pageData.get('descripcion').trim() === '') {
    dialogModule.alert({
      message: 'Por favor escriba un nombre del tema y su descripcion',
      okButtonText: 'OK'
    });
  }
  else{
    // Dismiss the keyboard
    page.getViewById('nombre').dismissSoftInput();
    page.getViewById('descripcion').dismissSoftInput();

    temaList.add(pageData.get('nombre'), pageData.get('descripcion'), pageData.get('usuario_creador'))
      .catch(function () {
        console.log(pageData.get('nombre'), pageData.get('descripcion'), pageData.get('usuario_creador'))
        dialogModule.alert({
          message: 'No se pudo guardar el tema...',
          okButtonText: 'OK'
        });
      });

    // Empty the input field
    pageData.set('nombre', '');
    pageData.set('descripcion', '');
    pageData.set('usuario_creador', '');
  }

};
