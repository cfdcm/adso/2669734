var dialogModule = require('@nativescript/core/ui/dialogs');
var config = require("../../shared/config");
var fetchModule = require("@nativescript/core/fetch");
var observableModule = require("@nativescript/core/data/observable");
var validator = require('email-validator');

function User(info) {
  console.log("========================= LOGIN ===========================================================")
  console.log("===========================================================================================")
    info = info || {};

    // You can add properties to observables on creation
    var viewModel = new observableModule.fromObject({
        username: info.username || "",
        password: info.password || "",
        groups: info.groups || "",
        policies: config.policies,
        ugc: config.ugc,

        user_id: info.user_id || "",
        email: info.email || "",
        nombre: info.nombre || "",
        apellido: info.apellido || "",
        groups: info.groups || "",
        is_active: info.is_active || "",
        is_staff: info.is_staff || "",
        token: info.token || ""
    });

    viewModel.login = function () {
        console.log(config.apiUrl)
        return fetchModule.fetch(config.apiUrl + "openscoco_interactivo/api/1.0/api_generate_token/", {
            method: "POST",
            body: JSON.stringify({
                username: viewModel.get("username"),
                password: viewModel.get("password")
            }),
            headers: getCommonHeaders()
        })
            .then(handleErrors)
            .then(function (response) {
                viewModel.set('isLoading', false);
                return response.json();
            })
            .then(function (data) {
              viewModel.token = data.token;
              config.token = 'Token ' + data.token;
            })
          ;
    };


    viewModel.register = function () {
        return fetchModule.fetch(config.apiUrl + "openscoco_interactivo/api/1.0/users/", {
            method: "POST",
            body: JSON.stringify({
                username: viewModel.get("username"),
                password: viewModel.get("password")
            }),
            headers: getCommonHeaders()
        }).then(handleErrors);
    };

    viewModel.obtainUserData = function () {
      console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
      console.log("+++++++++++++ Obtener los datos del Usuario +++++++++++++++++++++++++++++++++++++")

      return fetchModule.fetch(config.apiUrl + "openscoco_interactivo/api/1.0/obtain_user_data/", {
        method: "POST",
        body: JSON.stringify({
          username: viewModel.get("username"),
          password: viewModel.get("password")
        }),
        headers: {"Content-Type": "application/json"}
      })
        .then(handleErrors)
        .then(function (response) {
          //viewModel.set('isLoading', false);
          return response.json();
        })
        .then(function (data) {
          if (!data.is_active){
            throw Error("Usted no se encuentra activo, comuníquese con su Instructor...");
          }
          else {
            viewModel.set('user_id', data.user_id);
            viewModel.set('email', data.email);
            viewModel.set('nombre', data.first_name);
            viewModel.set('apellido', data.last_name);
            viewModel.set('is_active', data.is_active);
            viewModel.set('is_staff', data.is_staff);
            if (data.is_staff) {
              viewModel.set('groups', []);
              config.groups = data.groups
              config.rol = "ADMIN"
            }
            else{
              viewModel.set('groups', data.groups);
              config.groups = data.groups
              config.rol = data.groups[0].name
            }

            config.user_id = data.user_id
            config.nombre = data.first_name
            config.apellido = data.last_name


            //console.log("Datos capturados\n" + JSON.stringify(config))
            //console.log("----Dataaa capturados\n" + JSON.stringify(viewModel))
          }
        });
    };

    viewModel.isValidEmail = function () {
        var email = this.get('email');
        return validator.validate(email);
    };

    return viewModel;
}

function getCommonHeaders() {
    return {
        "Content-Type": "application/json",
        "Authorization": config.token
    }
}

function handleErrors(response) {
  console.log("Gestión de errores: STATUS: "+response.status + " ,TEXTO: "+response.statusText+ " ,FUNCIONA?: "+response.ok);
    if (!response.ok) {
        //console.log(JSON.stringify(response));
        throw Error(response.statusText);
    }
    return response;
}

module.exports = User;
