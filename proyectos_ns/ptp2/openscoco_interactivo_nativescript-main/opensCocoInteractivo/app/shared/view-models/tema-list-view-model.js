var dialogModule = require('@nativescript/core/ui/dialogs');
var config = require("../../shared/config");
var fetchModule = require("@nativescript/core/fetch");
var ObservableArray = require("@nativescript/core/data/observable-array").ObservableArray;
var frameModule = require('@nativescript/core/ui/frame');

function TemaListViewModel(items) {
    var baseUrl = config.apiUrl + "openscoco_interactivo/api/1.0/temas/";
    var viewModel = new ObservableArray(items);

    viewModel.load = function () {
        return fetchModule.fetch(baseUrl, {
            headers: getCommonHeaders()
        })
            .then(handleErrors)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
              console.log("Listando temas existentes")
              //console.log("Averiguar Rol para controles ------------"+JSON.stringify(config.rol))
              //console.log("======================================================")
              let oculto = ''
              let oculto2 = ''

                data.forEach((tema) => {
                  if (config.rol == "Aprendiz"){
                    oculto = 'collapsed'
                  }
                  else{
                    if (tema.usuario_creador == config.user_id) {
                      oculto = 'visible'
                      oculto2 = 'collapsed'
                    }
                    else{
                      oculto = 'collapsed'
                      oculto2 = 'visible'
                    }
                  }

                  viewModel.push({
                      id: tema.id,
                      nombre: tema.nombre,
                      descripcion: tema.descripcion,
                      usuario_creador: tema.usuario_creador,
                      //conteo: 'font://'+String.fromCharCode('0x3'+tema.conteo),
                      conteo: tema.conteo,
                      oculto: oculto,
                      oculto2: oculto2
                  });
                });
            });
    };

    viewModel.empty = function () {
        while (viewModel.length) {
            viewModel.pop();
        }
    };

    viewModel.add = function (nombre, descripcion, usuario_creador) {
      console.log("Petición de agregado: " + JSON.stringify({ nombre: nombre,  descripcion: descripcion, usuario_creador: usuario_creador}));
      console.log(baseUrl)
        return fetchModule.fetch(baseUrl, {
            method: "POST",
            body: JSON.stringify({ nombre: nombre,  descripcion: descripcion, usuario_creador: usuario_creador}),
            headers: getCommonHeaders()
        })
            .then(handleErrors)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
              dialogModule.alert({message: 'Registro guardado correctamente!!', okButtonText: 'OK' });
              /*viewModel.push({
                  id: data.id,
                  nombre: data.nombre,
                  descripcion: data.descripcion,
                  usuario_creador: data.usuario_creador,
                  puedeVer: "",
                  puedeVer2: ""
              });*/

              frameModule.Frame.topmost().navigate({moduleName: "temas/temas"});
            });
    }

    viewModel.delete = function (index) {
        // El slash al final es totalmente necesario.
        return fetchModule.fetch(baseUrl + viewModel.getItem(index).id + "/", {
          method: "DELETE",
          headers: getCommonHeaders()
        })
          .then(handleErrors)
          .then(function () {
            dialogModule.alert({message: 'Registro eliminado correctamente!!', okButtonText: 'OK' });
            viewModel.splice(index, 1);
          });
      }


    return viewModel;
}

function getCommonHeaders() {
    return {
        "Content-Type": "application/json",
        "Authorization": config.token
    }
}

function handleErrors(response) {
    console.log("Verificando posibles errores: " + JSON.stringify(response));
    if (!response.ok) {

        switch (response.status){
          case 400:
            dialogModule.alert({ title: "Error:", message: 'Revise que los datos de los campos estén correctos.', okButtonText: 'Aceptar' });
            break;
          case 401:
            dialogModule.alert({ title: "Información:", message: 'Su sesión expriró. Por favor inicie sesión nuevamente.', okButtonText: 'Salir' });
            // Enviar al usuario al login...
            const navigationOptions = {
              moduleName: "login/login",
            };
            frameModule.Frame.topmost().navigate(navigationOptions);
            break;
          case 403:
            dialogModule.alert({ title: "Error:", message: 'Usted no cuenta con los permisos para esta tarea...', okButtonText: 'Aceptar' });
            break;
          case 404:
            dialogModule.alert({ title: "Error:", message: 'Recurso no encontrado o fue eliminado por otro usuario.', okButtonText: 'Aceptar' });
            break;
          case 409:
            dialogModule.alert({ title: "Conflicto:", message: 'No puede eliminar el registro, porque tiene datos asociados.', okButtonText: 'Aceptar' });
            break;
          default:
            dialogModule.alert({ title: "Error:", message: '- '+response.statusText, okButtonText: 'Aceptar' });
            break;
        }
        throw Error(response.statusText);
    }
    return response;
}

module.exports = TemaListViewModel;
