var dialogModule = require('@nativescript/core/ui/dialogs');
var config = require("../../shared/config");
var fetchModule = require("@nativescript/core/fetch");
var observableModule = require("@nativescript/core/data/observable");
var validator = require('email-validator');
var frameModule = require('@nativescript/core/ui/frame');

function User(info) {
  console.log("========================= REGISTER ========================================================")
  console.log("===========================================================================================")
    info = info || {};

    // You can add properties to observables on creation
    var viewModel = new observableModule.fromObject({
        perfil: info.perfil || "2",
        perfil_name: info.perfil_name || "Aprendiz",
        first_name: info.first_name || "",
        last_name: info.last_name || "",
        email: info.email || "",
        username: info.username || "",
        password1: info.password1 || "",
        password2: info.password2 || ""
    });

    viewModel.isValidEmail = function () {
      var email = this.get('email');
      return validator.validate(email);
    };

    viewModel.register = function (data) {
      console.log("Petición de agregado: " + JSON.stringify(data));

      return fetchModule.fetch(config.apiUrl + "openscoco_interactivo/api/1.0/users/", {
        method: "POST",
        body: JSON.stringify(data),
        headers: getCommonHeaders()
      }).then(handleErrors);
    };

  return viewModel;
}

function getCommonHeaders() {
    // SPECIAL CASE: Es el token del usuario registrador, si cambia por alguna razón, no se podran crear usuarios.
    // Debe modificarse manualmente aquí... <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    return {
        "Content-Type": "application/json",
        "Authorization": "Token 38e41e65225376d7bca1685de2637132ac95b45b"
    }
}

function handleErrors(response) {
  console.log("Gestión de errores: STATUS: "+response.status + " ,TEXTO: "+response.statusText+ " ,FUNCIONA?: "+response.ok);
    if (!response.ok) {
      switch (response.status){
        case 400:
          dialogModule.alert({ title: "Error:", message: 'Ya existe un usuario con ese nombre.', okButtonText: 'Aceptar' });
          break;
        case 401:
          dialogModule.alert({ title: "Información:", message: 'En este momento no se permiten creación de más cuentas. Intente más tarde...', okButtonText: 'Salir' });
          // Enviar al usuario al login...
          const navigationOptions = {
            moduleName: "login/login",
          };
          frameModule.Frame.topmost().navigate(navigationOptions);
          break;
        default:
          dialogModule.alert({ title: "Error:", message: '- '+response.statusText, okButtonText: 'Aceptar' });
          break;
      }
      throw Error(response.statusText);
    }
    return response;
}

module.exports = User;
