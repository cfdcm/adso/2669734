var dialogModule = require('@nativescript/core/ui/dialogs');
var config = require("../../shared/config");
var fetchModule = require("@nativescript/core/fetch");
var ObservableArray = require("@nativescript/core/data/observable-array").ObservableArray;
var frameModule = require('@nativescript/core/ui/frame');

function SolucionListViewModel(items) {
    var baseUrl = config.apiUrl + "openscoco_interactivo/api/1.0/soluciones/";
    var viewModel = new ObservableArray(items);

    viewModel.load = function () {
        return fetchModule.fetch(baseUrl + "problema/"+config.id_problema+"/", {
            headers: getCommonHeaders()
        })
            .then(handleErrors)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
              //console.log("------------"+JSON.stringify(config))
              let oculto = ''
              let current_user_has_comment_or_star = false
              data.forEach((solucion) => {
                if (config.user_id == solucion.usuario_creador){
                  oculto = 'visible'
                }
                else{
                  oculto = 'collapsed'
                }

                let icon = '0x'
                let icono = ""
                let lenguaje = solucion.lenguaje_prog
                switch (lenguaje.toUpperCase()){
                  // los casos van en mayúsculas
                  case "JAVASCRIPT":
                    icono = icon + 'f3b8';
                    break;
                  case "PYTHON":
                    icono = icon + 'f3e2';
                    break;
                  case "JAVA":
                    icono = icon + 'f4e4';
                    break;
                  default:
                    icono = icon + 'f17a';
                }

                viewModel.push({
                  id: solucion.id,
                  problema: solucion.problema,
                  usuario_creador: solucion.usuario_creador_name + " " + solucion.usuario_creador_apell,
                  estrellas_promedio: String.fromCharCode('0xf0e7')+' '+(solucion.estrellas_promedio || 0),
                  webViewSrc: solucion.prettyCode,
                  lenguaje_prog: 'font://'+String.fromCharCode(icono),
                  fecha: solucion.fecha,
                  moderado: solucion.moderado_name,
                  oculto: oculto,
                  current_user_has_comment_or_star: solucion.current_user_has_comment_or_star[0],
                  id_comentario: solucion.current_user_has_comment_or_star[1],
                  calificacion_current_user: solucion.current_user_has_comment_or_star[2] || "",
                  comentario_current_user: solucion.current_user_has_comment_or_star[3] ? ('.-- ' + solucion.current_user_has_comment_or_star[3]) : "",
                  comentario_current_user_data: solucion.current_user_has_comment_or_star[3] || ""
                });
              });
            });
    };

    viewModel.empty = function () {
        while (viewModel.length) {
            viewModel.pop();
        }
    };

    viewModel.add = function (problema, usuario_creador, descripcion, lenguaje_prog, moderado) {
      console.log("Petición de agregado: " + JSON.stringify({ problema: problema,  usuario_creador: usuario_creador, descripcion: descripcion, lenguaje_prog: lenguaje_prog, moderado: moderado }));
      console.log(baseUrl)
        return fetchModule.fetch(baseUrl, {
            method: "POST",
            body: JSON.stringify({ problema: problema,  usuario_creador: usuario_creador, descripcion: descripcion, lenguaje_prog: lenguaje_prog, moderado: moderado }),
            headers: getCommonHeaders()
        })
            .then(handleErrors)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
              dialogModule.alert({message: 'Registro guardado correctamente!!', okButtonText: 'OK' });
              frameModule.Frame.topmost().navigate({moduleName: "soluciones/soluciones"});
            });
    }

    viewModel.add_estrellas = function (tipo_peticion, solucion, usuario_creador, calificacion, id_comentario) {
      console.log("Petición de agregado: " + JSON.stringify({ solucion: solucion,  usuario_creador: usuario_creador, calificacion: calificacion }));
      console.log(baseUrl)
      let URL = "";
      if (tipo_peticion == "POST"){
        URL = config.apiUrl + "openscoco_interactivo/api/1.0/comentarios/";
      }
      else if(tipo_peticion == "PUT"){
        URL = config.apiUrl + "openscoco_interactivo/api/1.0/comentarios/"+id_comentario+"/";
      }
      return fetchModule.fetch(URL, {
        method: tipo_peticion,
        body: JSON.stringify({ solucion: solucion,  usuario_creador: usuario_creador, calificacion: calificacion }),
        headers: getCommonHeaders()
      })
        .then(handleErrors)
        .then(function (response) {
          return response.json();
        })
        .then(function (data) {
          dialogModule.alert({message: 'Tus estrellas se guardaron correctamente!!', okButtonText: 'OK' });
          frameModule.Frame.topmost().navigate({moduleName: "soluciones/soluciones"});
        });
    }

  viewModel.add_comentarios = function (tipo_peticion, solucion, usuario_creador, comentario, id_comentario) {
    console.log("Petición de agregado: " + JSON.stringify({ solucion: solucion,  usuario_creador: usuario_creador, mensaje: comentario }));
    console.log(baseUrl)
    let URL = "";
    if (tipo_peticion == "POST"){
      URL = config.apiUrl + "openscoco_interactivo/api/1.0/comentarios/";
    }
    else if(tipo_peticion == "PUT"){
      URL = config.apiUrl + "openscoco_interactivo/api/1.0/comentarios/"+id_comentario+"/";
    }
    return fetchModule.fetch(URL, {
      method: tipo_peticion,
      body: JSON.stringify({ solucion: solucion,  usuario_creador: usuario_creador, mensaje: comentario }),
      headers: getCommonHeaders()
    })
      .then(handleErrors)
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        dialogModule.alert({message: 'Tu comentario se guardó correctamente!!', okButtonText: 'OK' });
        frameModule.Frame.topmost().navigate({moduleName: "soluciones/soluciones"});
      });
  }

    viewModel.delete = function (index) {
        // El slash al final es totalmente necesario.
        return fetchModule.fetch(baseUrl + viewModel.getItem(index).id + "/", {
          method: "DELETE",
          headers: getCommonHeaders()
        })
          .then(handleErrors)
          .then(function () {
            dialogModule.alert({message: 'Registro eliminado correctamente!!', okButtonText: 'OK' });
            viewModel.splice(index, 1);
          });
      }


    return viewModel;
}

function getCommonHeaders() {
    return {
        "Content-Type": "application/json",
        "Authorization": config.token
    }
}

function handleErrors(response) {
    console.log("Verificando posibles errores: " + JSON.stringify(response));
    if (!response.ok) {

        switch (response.status){
          case 400:
            dialogModule.alert({ title: "Error:", message: 'Revise que los datos de los campos estén correctos.', okButtonText: 'Aceptar' });
            break;
          case 401:
            dialogModule.alert({ title: "Información:", message: 'Su sesión expriró. Por favor inicie sesión nuevamente.', okButtonText: 'Salir' });
            // Enviar al usuario al login...
            const navigationOptions = {
              moduleName: "login/login",
            };
            frameModule.Frame.topmost().navigate(navigationOptions);
            break;
          case 403:
            dialogModule.alert({ title: "Error:", message: 'Usted no cuenta con los permisos para esta tarea...', okButtonText: 'Aceptar' });
            break;
          case 404:
            dialogModule.alert({ title: "Error:", message: 'Recurso no encontrado o fue eliminado por otro usuario.', okButtonText: 'Aceptar' });
            break;
          case 409:
            dialogModule.alert({ title: "Conflicto:", message: 'No puede eliminar el registro, porque tiene datos asociados.', okButtonText: 'Aceptar' });
            break;
          default:
            dialogModule.alert({ title: "Error:", message: '- '+response.statusText, okButtonText: 'Aceptar' });
            break;
        }
        throw Error(response.statusText);
    }
    return response;
}

module.exports = SolucionListViewModel;
