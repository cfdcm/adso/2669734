var dialogModule = require('@nativescript/core/ui/dialogs');
var config = require("../../shared/config");
var fetchModule = require("@nativescript/core/fetch");
var ObservableArray = require("@nativescript/core/data/observable-array").ObservableArray;
var frameModule = require('@nativescript/core/ui/frame');

function ComentarioListViewModel(items) {
    var baseUrl = config.apiUrl + "openscoco_interactivo/api/1.0/comentarios/";
    var viewModel = new ObservableArray(items);

    viewModel.load = function () {
        return fetchModule.fetch(baseUrl + "solucion/"+config.id_solucion+"/", {
            headers: getCommonHeaders()
        })
        .then(handleErrors)
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
          //console.log("------------"+JSON.stringify(config))
          let oculto = ''
          let current_user_has_comment_or_star = false
          data.forEach((comentario) => {
            viewModel.push({
              id: comentario.id,
              usuario_creador: comentario.usuario_creador_name + " " + comentario.usuario_creador_apell,
              mensaje: comentario.mensaje ? ('.-- ' + comentario.mensaje) : "",
              calificacion: comentario.calificacion || "-",
              moderado: comentario.moderado_name,
              fecha: comentario.fecha,
            });
          });
        });
    };

    viewModel.empty = function () {
        while (viewModel.length) {
            viewModel.pop();
        }
    };

    return viewModel;
}

function getCommonHeaders() {
    return {
        "Content-Type": "application/json",
        "Authorization": config.token
    }
}

function handleErrors(response) {
    console.log("Verificando posibles errores: " + JSON.stringify(response));
    if (!response.ok) {

        switch (response.status){
          case 400:
            dialogModule.alert({ title: "Error:", message: 'Revise que los datos de los campos estén correctos.', okButtonText: 'Aceptar' });
            break;
          case 401:
            dialogModule.alert({ title: "Información:", message: 'Su sesión expriró. Por favor inicie sesión nuevamente.', okButtonText: 'Salir' });
            // Enviar al usuario al login...
            const navigationOptions = {
              moduleName: "login/login",
            };
            frameModule.Frame.topmost().navigate(navigationOptions);
            break;
          case 403:
            dialogModule.alert({ title: "Error:", message: 'Usted no cuenta con los permisos para esta tarea...', okButtonText: 'Aceptar' });
            break;
          case 404:
            dialogModule.alert({ title: "Error:", message: 'Recurso no encontrado o fue eliminado por otro usuario.', okButtonText: 'Aceptar' });
            break;
          case 409:
            dialogModule.alert({ title: "Conflicto:", message: 'No puede eliminar el registro, porque tiene datos asociados.', okButtonText: 'Aceptar' });
            break;
          default:
            dialogModule.alert({ title: "Error:", message: '- '+response.statusText, okButtonText: 'Aceptar' });
            break;
        }
        throw Error(response.statusText);
    }
    return response;
}

module.exports = ComentarioListViewModel;
