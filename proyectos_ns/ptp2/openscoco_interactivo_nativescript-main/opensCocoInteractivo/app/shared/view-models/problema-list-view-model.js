var dialogModule = require('@nativescript/core/ui/dialogs');
var config = require("../../shared/config");
var fetchModule = require("@nativescript/core/fetch");
var ObservableArray = require("@nativescript/core/data/observable-array").ObservableArray;
var frameModule = require('@nativescript/core/ui/frame');

function ProblemaListViewModel(items) {
    var baseUrl = config.apiUrl + "openscoco_interactivo/api/1.0/problemas/";
    var viewModel = new ObservableArray(items);

    viewModel.load = function () {
        return fetchModule.fetch(baseUrl + "tema/"+config.id_tema+"/", {
            headers: getCommonHeaders()
        })
            .then(handleErrors)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
              //console.log("------------"+JSON.stringify(config))
              let oculto = ''
              let oculto2 = ''

              data.forEach((problema) => {
                if (config.user_id == problema.usuario_creador){
                  oculto = 'visible'
                  oculto2 = 'collapsed'
                }
                else{
                  oculto = 'collapsed'
                  oculto2 = 'visible'
                }

                let icon = '0x'
                let icono = ""
                let nivel = problema.nivel
                switch (nivel){
                  case "F":
                    //icono = icon + 'f243';
                    icono = icon + 'f525';
                    break;
                  case "M":
                    //icono = icon + 'f241';
                    icono = icon + 'f528';
                    break;
                  case "D":
                    //icono = icon + 'f240';
                    icono = icon + 'f527';
                    break;
                  default:
                    //icono = icon + 'f244';
                    icono = icon + 'f522';
                }

                viewModel.push({
                    id: problema.id,
                    tema: problema.tema,
                    titulo: problema.titulo || "--Sin título--",
                    descripcion: problema.descripcion,
                    fecha: problema.fecha,
                    usuario_creador: problema.usuario_creador,
                    usuario_creador_name: problema.usuario_creador_name+" "+problema.usuario_creador_apell,
                    moderado: problema.moderado,
                    moderado_name: problema.moderado_name,
                    nivel: 'font://'+String.fromCharCode(icono),
                    conteo: problema.conteo,
                    oculto: oculto,
                    oculto2: oculto2,
                    nivel_choices: problema.nivel || "-",
                    nivel_name: problema.nivel_name || "Sin categorizar",
                    control_nivel: config.rol == 'Instructor' ? true : false,
                    control_moderador: config.rol == 'Instructor' ? true : false
                });
              });
            });
    };

    viewModel.empty = function () {
        while (viewModel.length) {
            viewModel.pop();
        }
    };

    viewModel.add = function (tema, usuario_creador, titulo, descripcion, moderado, nivel) {
      console.log("Petición de agregado: " + JSON.stringify({ titulo: titulo,  descripcion: descripcion, nivel: nivel, tema: tema, usuario_creador: usuario_creador, moderado: moderado }));
      console.log(baseUrl)
        return fetchModule.fetch(baseUrl, {
            method: "POST",
            body: JSON.stringify({ titulo: titulo,  descripcion: descripcion, nivel: nivel, tema: tema, usuario_creador: usuario_creador, moderado: moderado }),
            headers: getCommonHeaders()
        })
            .then(handleErrors)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
              dialogModule.alert({message: 'Registro guardado correctamente!!', okButtonText: 'OK' });
              /*viewModel.push({
                  id: problema.id,
                  tema: problema.tema,
                  usuario_creador: problema.usuario_creador,
                  titulo: problema.titulo,
                  descripcion: problema.descripcion,
                  fecha: problema.fecha,
                  moderado: problema.moderado,
                  nivel: problema.nivel,
                  puedeVer: ""
              });*/

              frameModule.Frame.topmost().navigate({moduleName: "problemas/problemas"});
            });
    };

    viewModel.delete = function (index) {
      // El slash al final es totalmente necesario.
      return fetchModule.fetch(baseUrl + viewModel.getItem(index).id + "/", {
        method: "DELETE",
        headers: getCommonHeaders()
      })
        .then(handleErrors)
        .then(function () {
          dialogModule.alert({message: 'Registro eliminado correctamente!!', okButtonText: 'OK' });
          viewModel.splice(index, 1);
        });
    };

  viewModel.cambio_nivel = function (id, tema, usuario_creador, descripcion, nivel) {
    console.log("Petición de modificación: " + JSON.stringify({ tema: tema, usuario_creador: usuario_creador, descripcion: descripcion, nivel: nivel }));

    let URL = "";

    URL = baseUrl +id+"/";
    console.log(URL)
    return fetchModule.fetch(URL, {
      method: "PUT",
      body: JSON.stringify({ tema: tema, usuario_creador: usuario_creador, descripcion: descripcion, nivel: nivel }),
      headers: getCommonHeaders()
    })
      .then(handleErrors)
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        dialogModule.alert({message: 'Se estableció correctamente el nivel!!', okButtonText: 'OK' });
        frameModule.Frame.topmost().navigate({moduleName: "problemas/problemas"});
      });
  }

  viewModel.cambio_estado = function (id, tema, usuario_creador, descripcion, estado) {
    console.log("Petición de modificación: " + JSON.stringify({ tema: tema, usuario_creador: usuario_creador, descripcion: descripcion, moderado: estado }));

    let URL = "";

    URL = baseUrl +id+"/";
    console.log(URL)
    return fetchModule.fetch(URL, {
      method: "PUT",
      body: JSON.stringify({ tema: tema, usuario_creador: usuario_creador, descripcion: descripcion, moderado: estado }),
      headers: getCommonHeaders()
    })
      .then(handleErrors)
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        dialogModule.alert({message: 'Se estableció correctamente el estado!!', okButtonText: 'OK' });
        frameModule.Frame.topmost().navigate({moduleName: "problemas/problemas"});
      });
  }

  return viewModel;
}

function getCommonHeaders() {
    return {
        "Content-Type": "application/json",
        "Authorization": config.token
    }
}

function handleErrors(response) {
    console.log("Verificando posibles errores: " + JSON.stringify(response));
    if (!response.ok) {

        switch (response.status){
          case 400:
            dialogModule.alert({ title: "Error:", message: 'Revise que los datos de los campos estén correctos.', okButtonText: 'Aceptar' });
            break;
          case 401:
            dialogModule.alert({ title: "Información:", message: 'Su sesión expriró. Por favor inicie sesión nuevamente.', okButtonText: 'Salir' });
            // Enviar al usuario al login...
            const navigationOptions = {
              moduleName: "login/login",
            };
            frameModule.Frame.topmost().navigate(navigationOptions);
            break;
          case 403:
            dialogModule.alert({ title: "Error:", message: 'Usted no cuenta con los permisos para esta tarea...', okButtonText: 'Aceptar' });
            break;
          case 404:
            dialogModule.alert({ title: "Error:", message: 'Recurso no encontrado o fue eliminado por otro usuario.', okButtonText: 'Aceptar' });
            break;
          case 409:
            dialogModule.alert({ title: "Conflicto:", message: 'No puede eliminar el registro, porque tiene datos asociados.', okButtonText: 'Aceptar' });
            break;
          default:
            dialogModule.alert({ title: "Error:", message: '- '+response.statusText, okButtonText: 'Aceptar' });
            break;
        }
        throw Error(response.statusText);
    }
    return response;
}

module.exports = ProblemaListViewModel;
