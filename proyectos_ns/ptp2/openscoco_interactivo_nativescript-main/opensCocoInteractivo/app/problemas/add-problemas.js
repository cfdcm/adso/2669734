var dialogModule = require('@nativescript/core/ui/dialogs');
var observableModule = require('@nativescript/core/data/observable');
var ProblemaListViewModel = require('../shared/view-models/problema-list-view-model');
var frameModule = require('@nativescript/core/ui/frame');
var config = require("../shared/config");

var page;
var problemaList = new ProblemaListViewModel([]);
var pageData = new observableModule.fromObject({
    problemaList: problemaList,
    tema: '',
    usuario_creador: config.user_id,
    titulo: '',
    description: '',
    fecha: '',
    moderado: 'C',
    nivel: '-',
    puedeVer: ""
});

exports.regresar = function (args) {
  console.log("Regresar a PROBLEMAS del TEMA x...")
  frameModule.Frame.topmost().navigate({ moduleName: "problemas/problemas" });
};

exports.loaded = function (args) {
    page = args.object;
    page.bindingContext = pageData;

    pageData.set('user', "Bienvenido ("+config.rol+"), "+config.nombre);
    pageData.set('movimiento', "TEMA: "+config.id_tema_nombre);
    pageData.set('tema', config.id_tema);
    pageData.set('usuario_creador', config.user_id);

    console.log(" =========== ADD PROBLEMAS Page =============")
};

exports.add = function () {
  // Check for empty submission
  if (pageData.get('titulo').trim() === '' || pageData.get('descripcion').trim() === '') {
    dialogModule.alert({
      message: 'Por favor escriba un título del problema y su descripcion',
      okButtonText: 'OK'
    });
  }
  else{
    // Dismiss the keyboard
    page.getViewById('titulo').dismissSoftInput();
    page.getViewById('descripcion').dismissSoftInput();

    console.log("DATOS PETT" + pageData.get('tema'), pageData.get('usuario_creador'), pageData.get('titulo'), pageData.get('descripcion'), pageData.get('moderado'), pageData.get('nivel'))
    problemaList.add(pageData.get('tema'), pageData.get('usuario_creador'), pageData.get('titulo'), pageData.get('descripcion'), pageData.get('moderado'), pageData.get('nivel'))
      .catch(function () {

        dialogModule.alert({
          message: 'No se pudo guardar el problema...',
          okButtonText: 'OK'
        });
      });

    // Empty the input field
    pageData.set('titulo', '');
    pageData.set('descripcion', '');
  }

};

