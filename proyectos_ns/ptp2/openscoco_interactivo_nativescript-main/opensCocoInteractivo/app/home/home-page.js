var frameModule = require('@nativescript/core/ui/frame');

var observableModule = require('@nativescript/core/data/observable')

var page;
var email;

// Creamos un objeto usuario para pasarlo a la página en el evento loaded
var user = new observableModule.fromObject({
  email: "jor@misena.edu.co",
  password: "12345"
});

// Funcionalidades de carga inicial del .xml y los eventos de los botones

exports.loaded = function(args){
  page = args.object;
  page.bindingContext = user;
};

exports.signIn = function(){
  email = page.getViewById('email');
  console.log(email.text);
};

exports.register = function(args){
  const navigationOptions = {
    moduleName: "register/register-page",
    context: {
      'info': "Bienvenido: " + user.email,
      'resultado1': "",
      'resultado2': ""
    },
    //,context: { bindingContext: viewModel }
    animated: true,
    // Set up a transition property on page navigation.
    transition: {
        name: "slide",
        duration: 380,
        curve: "easeIn"
    }
  };
  args.object.page.frame.navigate(navigationOptions);
};

// Funcionalidades de carga inicial del .xml y los eventos de los botones -FIN
