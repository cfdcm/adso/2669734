var dialogModule = require('@nativescript/core/ui/dialogs');
var observableModule = require('@nativescript/core/data/observable');
var ObservableArray = require('@nativescript/core/data/observable-array').ObservableArray;
var ComentarioListViewModel = require('../shared/view-models/comentario-list-view-model');
var frameModule = require('@nativescript/core/ui/frame');
var config = require("../shared/config");

var page;

var comentarioList = new ComentarioListViewModel([]);

var pageData = new observableModule.fromObject({
  comentarioList: comentarioList,
  usuario_creador: '',
  mensaje: '',
  calificacion: '',
  moderado: '',
  fecha: '',
  mostrar: false,
  mostrar2: false
});

exports.regresar = function (args) {
  console.log("Regresar al listado de soluciones del problema..."+config.id_solucion_problema)
  frameModule.Frame.topmost().navigate({ moduleName: "soluciones/soluciones" });
};

exports.loaded = function (args) {
    page = args.object;

    var listView = page.getViewById('comentarioList');
    page.bindingContext = pageData;

    comentarioList.empty();
    pageData.set('isLoading', true);
    comentarioList.load().then(function () {
        pageData.set('isLoading', false);
        listView.animate({
            opacity: 1,
            duration: 1000
        });
    });

    pageData.set('user', "Bienvenido ("+config.rol+"), "+config.nombre);
    pageData.set('tema_seleccionado', "TEMA: "+config.id_tema_nombre);
    pageData.set('problema_seleccionado', "PROBLEMA: ");

    pageData.set('solucion_seleccionada', "Comentarios de SOLUCION: ");

    pageData.set('titul', config.id_problema_titulo);
    pageData.set('problema', config.id_problema_descripcion);
    pageData.set('solucion', config.id_solucion_problema);

};



// disabling the WebView's zoom control
exports.onWebViewLoaded = function(webargs) {
  const webview = webargs.object;
  webview.android.getSettings().setDisplayZoomControls(false);
}


exports.toggleProblema = function() {
  console.log("Estado: "+pageData.mostrar);
  if (pageData.mostrar){
    pageData.set('mostrar', false);
  }
  else{
    pageData.set('mostrar', true);
  }
}


exports.toggleSolucion = function() {
  console.log("Estado2: "+pageData.mostrar2);
  if (pageData.mostrar2){
    pageData.set('mostrar2', false);
  }
  else{
    pageData.set('mostrar2', true);
  }
}
