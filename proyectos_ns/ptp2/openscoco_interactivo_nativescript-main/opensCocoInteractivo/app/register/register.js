var dialogModule = require('@nativescript/core/ui/dialogs');
var observableModule = require('@nativescript/core/data/observable');
var ObservableArray = require('@nativescript/core/data/observable-array').ObservableArray;
var RegisterUserViewModel = require('../shared/view-models/register-view-model');
var frameModule = require('@nativescript/core/ui/frame');
var config = require("../shared/config");
var validator = require('email-validator');

var registeruser = new RegisterUserViewModel({
  first_name: "",
  last_name: "",
  email: "",
  username: "",
  password1: "",
  password2: ""
});

var page;

exports.regresar = function (args) {
  console.log("Regresar al login...")
  frameModule.Frame.topmost().navigate({ moduleName: "login/login" });
};

exports.loaded = function (args) {
  page = args.object;

  registeruser.set('perfil_name', 'Aprendiz');

  page.bindingContext = registeruser;
  console.log(" =========== REGISTER Page =============")
};

exports.perfilDialog = function() {
  const actionOptions = {
    title: "Perfil del Usuario",
    message: "Seleccione el tipo de usuario",
    cancelButtonText: "Cancelar",
    actions: ["Aprendiz", "Instructor"],
    cancelable: true // Android only
  };

  action(actionOptions).then((result) => {
    console.log(result);
    if (result == "Aprendiz"){
      registeruser.set('perfil', 2);
      registeruser.set('perfil_name', result);
    }
    else if (result == "Instructor"){
      dialogModule.alert({ title: "Información:", message: 'Debe ingresar su correo @sena.edu.co para validar su solicitud.', okButtonText: 'Aceptar' });
      registeruser.set('perfil', 3);
      registeruser.set('perfil_name', result);
    }
  });
};

function completeRegistration(username, email, first_name, last_name, is_active, password, groups) {

  registeruser.register({username: username, email: email, first_name: first_name, last_name: last_name, is_active: is_active, password: password, groups: [parseInt(groups)] })
    .then(function () {
      dialogModule
        .alert({ title: "Confirmación:", message: 'Cuenta creada correctamente!!', okButtonText: 'Aceptar' })
        .then(function () {
          frameModule.Frame.topmost().navigate("login/login");
        });
    }).catch(function (error) {
    console.log(error);
    dialogModule.alert({ title: "Error:", message: 'No fue posible crear su cuenta.', okButtonText: 'Aceptar' });

  });
}

exports.register = function () {

  if (registeruser.first_name.trim() === '' || registeruser.last_name.trim() === ''){
    dialogModule.alert({ title: "Error:", message: 'Ingrese su nombre y apellido...', okButtonText: 'Aceptar' });
    page.getViewById('first_name').focus();
  }
  else if (!registeruser.isValidEmail()){
    dialogModule.alert({ title: "Error:", message: 'Ingrese un email válido.', okButtonText: 'Aceptar' });
    page.getViewById('email').focus();
  }
  else if (registeruser.username.trim() === '' || registeruser.username.length < 3){
    dialogModule.alert({ title: "Error:", message: 'Ingrese un nombre de usuario válido.', okButtonText: 'Aceptar' });
    page.getViewById('username').focus();
  }
  else if ((registeruser.password1.trim() === '' || registeruser.password2.trim() === '') || (registeruser.password1.trim() != registeruser.password2.trim() )){
    dialogModule.alert({ title: "Error:", message: 'Sus contraseñas no coinciden o no la ingresó...', okButtonText: 'Aceptar' });
    page.getViewById('password1').focus();
  }
  else{
    console.log("Datos correctos...")

    completeRegistration(
      registeruser.username.trim(),
      registeruser.email.trim(),
      registeruser.first_name.trim(),
      registeruser.last_name.trim(),
      true,
      registeruser.password1.trim(),
      registeruser.perfil,
    );
  }
};
