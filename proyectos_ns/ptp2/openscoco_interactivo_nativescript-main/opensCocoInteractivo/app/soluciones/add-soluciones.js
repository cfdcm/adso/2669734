var dialogModule = require('@nativescript/core/ui/dialogs');
var observableModule = require('@nativescript/core/data/observable');
var SolucionListViewModel = require('../shared/view-models/solucion-list-view-model');
var frameModule = require('@nativescript/core/ui/frame');
var config = require("../shared/config");

var page;
var solucionList = new SolucionListViewModel([]);
var pageData = new observableModule.fromObject({
    solucionList: solucionList,
    problema_desc: '',
    problema: '',
    usuario_creador: '',
    descripcion: '',
    lenguaje_prog: '',
    moderado: 'C',
    fecha: '',
    puedeVer: "",
    mostrar: false
});

exports.regresar = function (args) {
  console.log("Regresar a SOLUCIONES del PROBLEMA x...")
  frameModule.Frame.topmost().navigate({ moduleName: "soluciones/soluciones" });
};

exports.loaded = function (args) {
    page = args.object;
    page.bindingContext = pageData;

    pageData.set('user', "Bienvenido ("+config.rol+"), "+config.nombre);
    pageData.set('tema_seleccionado', "TEMA: "+config.id_tema_nombre);
    pageData.set('problema_seleccionado', "Soluciones del PROBLEMA: ");

    pageData.set('titul', config.id_problema_titulo);
    pageData.set('problema_desc', config.id_problema_descripcion);

    pageData.set('problema', config.id_problema);
    pageData.set('usuario_creador', config.user_id);

  console.log(" =========== ADD SOLUCIONES Page =============")
};

exports.add = function () {
  // Check for empty submission
  if (pageData.get('lenguaje_prog').trim() === '' || pageData.get('descripcion').trim() === '') {
    dialogModule.alert({
      message: 'Por favor escriba el nombre del Lenguaje de Programación y su Código que resuelva el problema.',
      okButtonText: 'OK'
    });
  }
  else{
    // Dismiss the keyboard
    page.getViewById('lenguaje_prog').dismissSoftInput();
    page.getViewById('descripcion').dismissSoftInput();

    console.log("DATOS PETT" + pageData.get('problema'), pageData.get('usuario_creador'), pageData.get('descripcion'), pageData.get('lenguaje_prog'), pageData.get('moderado'))
    solucionList.add(pageData.get('problema'), pageData.get('usuario_creador'), pageData.get('descripcion'), pageData.get('lenguaje_prog'), pageData.get('moderado'))
      .catch(function () {

        dialogModule.alert({
          message: 'No se pudo guardar la solución al problema...',
          okButtonText: 'OK'
        });
      });

    // Empty the input field
    pageData.set('lenguaje_prog', '');
    pageData.set('descripcion', '');
  }

};

exports.toggleProblema = function() {
  console.log("Estado: "+pageData.mostrar);
  if (pageData.mostrar){
    pageData.set('mostrar', false);
  }
  else{
    pageData.set('mostrar', true);
  }
}
