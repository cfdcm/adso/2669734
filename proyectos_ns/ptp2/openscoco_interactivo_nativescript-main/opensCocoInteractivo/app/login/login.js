var frameModule = require('@nativescript/core/ui/frame');
var UserViewModel = require('../shared/view-models/user-view-model');
var dialogModule = require('@nativescript/core/ui/dialogs');
var config = require("../shared/config");

var user = new UserViewModel({
    username: '',
    password: '',
    ugc: 'Cancelar'
});
var page;

exports.loaded = function (args) {
  console.log("Estado de UGC: "+config.ugc)
  console.log("Estado de UGC policies: "+config.policies)
    page = args.object;

    if (page.ios) {
        var navigationBar = frameModule.Frame.topmost().ios.controller.navigationBar;
        navigationBar.barStyle = UIBarStyle.UIBarStyleBlack;
    }
    user.set("ugc", config.ugc);
    page.bindingContext = user;
}


exports.aceptarPoliticasUGC = function (args) {
  console.log("Estado de UGC: "+config.ugc)
  console.log("Estado de UGC policies: "+config.policies)
  var switche = page.getViewById('switche');
  console.log("-- Nuevo Estado: "+switche.checked)
  if (switche.checked && config.policies==false){
    console.log("Activar botones")

    config.ugc = 'Aceptar';
    config.policies = true;
    user.set("ugc", config.ugc);

    var login_button = page.getViewById('login_button');
    login_button.isEnabled = true
    var register_button = page.getViewById('register_button');
    register_button.isEnabled = true
  }
  else if (!switche.checked && config.policies==true){
    console.log("Desactivar botones")

    config.ugc = 'Cancelar';
    config.policies = false;
    user.set("ugc", config.ugc);

    var login_button = page.getViewById('login_button');
    login_button.isEnabled = false
    var register_button = page.getViewById('register_button');
    register_button.isEnabled = false
  }
  else{
    console.log("No se detectó estado de aceptación de políticas de uso UGC.")
  }

}



const modalView = "login/modal-root";
//const modalViewModule = "login/modal-view-page";
//const modalViewModule = "ns-ui-category/modal-view/basics/modal-view-page";

exports.openModal = function (args) {
  const mainpage = args.object.page;
  const option = {
    context: "some context",
    closeCallback: () => {},
    fullscreen: false,
    animated: true
  };
  mainpage.showModal(modalView, option);
}


exports.signIn = function () {
    user.set('isLoading', true);
    user.login()
        .catch(function (error) {
            user.set('isLoading', false);
            console.log("Error en login >>>" + error);
            if (error == "Error: Bad Request"){
              dialogModule.alert({
                title: "Error:",
                message: 'Usuario o contraseña incorrectos...',
                okButtonText: 'OK'
              });
            }
            else if (error == "Error: Unauthorized"){
              dialogModule.alert({
                title: "Error:",
                message: 'Usted no se encuentra activo, comuníquese con su Instructor...',
                okButtonText: 'OK'
              });
            }
            else{
              user.set('isLoading', false);
              dialogModule.alert({
                title: "Error:",
                message: "No se pudo conectar con el servidor... ",
                okButtonText: 'OK'
              });
            }

            return Promise.reject();
        })
        .then(function () {
          console.log("Averiguar datos del usuario on TOKEN------------ Navegar: "+ user.token)
          let rol;
          let rol_name;
          let nombre;
          //aquí debo consultar los datos del usuario para pasar contexto a la otra pagina.
          user.obtainUserData()
            .catch(function (error) {
              console.log("Error consultando ROL:" + error)
            })
            .then(function () {
              nombre = user.nombre;

              if (user.is_staff){
                rol = 0;
                rol_name= "ADMIN";
                console.log("Datos de Usuario GRUPOS: ADMIN")
              }
              else{
                rol = user.groups[0].id;
                rol_name = user.groups[0].name;
              }

              const navigationEntry = {
                moduleName: "temas/temas",
                context: {
                  username: nombre,
                  rol: rol,
                  rol_name: rol_name
                },
                animated: false
              };

              frameModule.Frame.topmost().navigate(navigationEntry);
              console.log("======== USUARIO LOGUEADO CORRECTAMENTE -> TEMAS =========")

            });


        });
}

exports.register = function () {
    var topmost = frameModule.Frame.topmost();
    topmost.navigate('register/register');
}

const { exit } = require("nativescript-exit");
exports.salir = function(){
  exit();
}
