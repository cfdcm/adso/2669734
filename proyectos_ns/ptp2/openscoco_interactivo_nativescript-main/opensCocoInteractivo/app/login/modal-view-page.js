const Observable = require("@nativescript/core/data/observable").Observable;
const observableModule = require("@nativescript/core/data/observable")
const dialogs = require("@nativescript/core/ui/dialogs");
const webViewModule = require("@nativescript/core/ui/web-view");

var datos = observableModule.fromObject({
  estado : true,
  webViewSrc: "http://openscoco.tikole.net/openscoco_interactivo/?no_cache="+String(Math.random())
});

function onPageLoaded(args) {
  console.log("Carga de Popup");
  const page = args.object;

  datos.set("webViewSrc", datos.webViewSrc );
  datos.set("estado", true);

  page.bindingContext = datos;
}
exports.onPageLoaded = onPageLoaded;

function onWebViewLoaded(webargs) {

  const webview = webargs.object;
  webview.on(webViewModule.WebView.loadFinishedEvent, (args) => {
    let message = "";
    if (!args.error) {
      message = `Página terminó carga.... ${args.url}`;

    } else {
      message = `Error loading ${args.url} : ${args.error}`;
      datos.set("webViewSrc", "Ocurrió un problema:<br><br>Intente de nuevo más tarde.");
    }
    datos.set("estado", false);
    console.log(`WebView message - ${message}`);
  });

}
exports.onWebViewLoaded = onWebViewLoaded;


function onCloseModal(args) {
  console.log("Cerrando modal...")
  args.object.closeModal();
}
exports.onCloseModal = onCloseModal;
