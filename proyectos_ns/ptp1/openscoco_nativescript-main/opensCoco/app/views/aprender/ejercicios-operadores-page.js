
const { fromObject } = require("@nativescript/core/data/observable")


function ready(args) {

    const page = args.object;
    const vm = fromObject({
        // Setting the listview binding source
        myTitles: [
            {
                "title": "Ejercicio 1",
                "desc": "Realizar un algoritmo que sume dos números ",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/esnh651p5tojzm7/A1.png?dl=1"
            },
            {
                "title": "Ejercicio 2",
                "desc": "Determinar la hipotenusa de un triángulo rectángulo conocidas las longitudes de sus dos catetos. Desarrolle el algoritmo correspondiente. \nNota: La hipotenusa de un triángulo rectángulo es igual a la raíz cuadrada de la suma del cuadrado de los catetos.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/6bx6i6nogd8sgbp/A2.png?dl=1"
            },
            {
                "title": "Ejercicio 3",
                "desc": "Desarrollar un algoritmo que calcule el área de un cuadrado. \nNota: Es el producto de la base por la altura del cuadrado",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/4lm4xb902nx0adx/A3.png?dl=1"
            },
            {
                "title": "Ejercicio 4",
                "desc": "Desarrolle un algoritmo que permita determinar el área y volumen de un cilindro dado su radio (R) y altura (H) en centímetros. \nNota: El volumen de un cilindro es π r² h, y el área de su superficie es 2π r h + 2π r²",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/5ebxnsy7959gakx/A4.png?dl=1"
            },
            {
                "title": "Ejercicio 5",
                "desc": "Encuentre el área total de superficie de un cilindro con una base de radio de 5 pulgadas y una altura de 7 pulgadas.\nNota: El área de su superficie es 2π r h + 2π r²",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/vynmemyzciq7nhc/A5.png?dl=1"
            },
            {
                "title": "Ejercicio 6",
                "desc": "Realiza un algoritmo que le permita determinar el área de un rectángulo \nNota: Es el producto de la base por la altura del rectángulo.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/fuy6jbxa0ih8zp9/A6.png?dl=1"
            },
            {
                "title": "Ejercicio 7",
                "desc": "Realice un algoritmo que determine cuantos minutos hay en 5 horas ",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/66d3d0ge6it70d8/A7.png?dl=1"
            },
            {
                "title": "Ejercicio 8",
                "desc": "Realice un algoritmo que a partir de proporcionarle la velocidad de un automóvil, expresada en kilómetros por hora, proporcione la velocidad en metros por segundo. \nNota: La fórmular es: (Vel * 1000) / 3600 ",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/6929ha5hy9xdtx0/A8.png?dl=1"
            },
            {
                "title": "Ejercicio 9",
                "desc": "Desarrolle un algoritmo que lea la velocidad en metros por segundo y la convierta a kilómetros por hora. \nNota: La fórmula es: (Vel * 3600) / 1000 ",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/o6j71clsfttop63/A9.png?dl=1"
            },
            {
                "title": "Ejercicio 10",
                "desc": "Desarrolle un algoritmo que funcione como caja registradora. Ingrese “Código de Producto y \nPrecio:”. La salida de debe parecer a una factura incluyendo el cálculo del IVA del 19%.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/l4y0wjrz82t0mdi/A10.png?dl=1"
            },
            {
                "title": "Ejercicio 11",
                "desc": "Realice un algoritmo de donde muestre su nombre y su apellido.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/ehdmfm87o2g4qy8/A11.png?dl=1"
            },
            {
                "title": "Ejercicio 12",
                "desc": "Realice un algoritmo que calcule la raíz cuadrada de un número",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/wcbrzhva0ych8bz/A12.png?dl=1"
            },
            {
                "title": "Ejercicio 13",
                "desc": "Realice un algoritmo que calcule el área de un rombo la fórmula es: A = (D*d)/2.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/n7svctmj8ojyphy/A13.png?dl=1"
            },
            {
                "title": "Ejercicio 14",
                "desc": "Realice un algoritmo que calcule el área de un trapecio la fórmula es: A = ((B + b).h)/2",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/pilkkemcjnt2qeg/A14.png?dl=1"
            },
            {
                "title": "Ejercicio 15",
                "desc": "Realice un algoritmo de una calculadora que realice las operaciones básicas: suma, resta, multiplicación y división.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/f837jxxqdw75e8r/A15.png?dl=1"
            },
            {
                "title": "Ejercicio 16",
                "desc": "Realice un algoritmo que calcule el perímetro de un triángulo, la fórmula es: P = a + b + c.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/g86l8o78a67wbgp/A16.png?dl=1"
            },
            {
                "title": "Ejercicio 17",
                "desc": "Realice un algoritmo que registre y muestre la solicitud de 5 productos de un supermercado.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/mkhq90bgx1gnbn4/A17.png?dl=1"
            },
            {
                "title": "Ejercicio 18",
                "desc": "Realice un algoritmo que calcule el porcentaje de un número.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/jw59x9r651e6bhg/A18.png?dl=1"
            },
            {
                "title": "Ejercicio 19",
                "desc": "Realice un algoritmo donde se calcule el sueldo de un trabajador, cuando el mismo devenga por día $15.000. Y el trabajo que la persona realiza no es continuo.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/dysehziph1fh6f5/A19.png?dl=1"
            },
            {
                "title": "Ejercicio 20",
                "desc": "Realice un algoritmo para identificar una palabra palíndroma.\nEjemplo: somos -> al revés es : somos ",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/diahli364g4t35j/A20.png?dl=1"
            },
            {
                "title": "Ejercicio 21",
                "desc": "Realice un algoritmo para identificar un número es capicúa.\nEjemplo: 1001 -> al revés es: 1001",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/0bce629d185djp3/A21.png?dl=1"
            },
            {
                "title": "Ejercicio 22",
                "desc": "Dadas dos variables numéricas A y B, se pide realizar un algoritmo que intercambie los valores de ambas variables y muestre que valor toman finalmente las dos variables.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/24aqjjw88k32nnh/A22.png?dl=1"
            },
            {
                "title": "Ejercicio 23",
                "desc": "Algoritmo que lea dos números, calculando y escribiendo el valor de su suma, resta, producto y división.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/mza6sykv9b6gx9x/A23.png?dl=1"
            },
            {
                "title": "Ejercicio 24",
                "desc": "Realizar un programa que convierta los grados a radianes.\nNota: 1° × π/180 = 0,01745rad",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/xi5khl74t1yzspe/A24.png?dl=1"
            },
            {
                "title": "Ejercicio 25",
                "desc": "Realizar un algoritmo que pida un valor entero que equivale a un número de pesos y me calcule a cuantos billetes de $100.000, $50.000, $20.000, $10.000, $5.000, $2.000, $1.000 o monedas de 500, 200, 100, 50 ó 1 lo representan",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/c4g1y5w3wefxuit/A25.png?dl=1"
            },
            {
                "title": "Ejercicio 26",
                "desc": "Realizar un programa que pida al usuario la velocidad en m/s y el radio de la circunferencia de la pista, y resultada-- el programa devuelve el tiempo que tarda el atleta en dar 2 vueltas a la pista, sabiendo que el atleta descansa 1 minuto cada 1000 metros.\nNota1: Longitud = 2 π *r -> tener en cuenta que son 2 vueltas. \nNota2: Descanso = (Longitud / 1000)*60 segundos\nFórmula: Velocidad = distancia/tiempo -> tiempo = (distancia/Velocidad), no olvide el descanso.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/5j3247bvh6t7pp9/A26.png?dl=1"
            },
            {
                "title": "Ejercicio 27",
                "desc": "¿Cuáles y cuántos son los números primos comprendidos entre 1 y 1000? \nNota: Número primo es aquel que solo es divisible por si mismo y por la unidad",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/ozw647go1e9zm3d/A27.png?dl=1"
            },
            {
                "title": "Ejercicio 28",
                "desc": "Leer una nota y escribir en pantalla la calificación en palabras. Notas  de 1 a 5.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/yzubgzraz5vt80c/A28.png?dl=1"
            },
            {
                "title": "Ejercicio 29",
                "desc": "Leer una nota y escribir en pantalla la calificación en palabras. Notas  de 1 a 100.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/3snutsfbtxhlzvf/A29.png?dl=1"
            },
            {
                "title": "Ejercicio 30",
                "desc": "Construya un algoritmo que conocida la base y altura de un rectángulo, calcule y muestre su área y perímetro. \nNota: Perímetro = 4L.  Área = (D*d)/2  D:Diagonal mayor, d:Diagonal menor",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/pga8949bvkd098y/A30.png?dl=1"
            },
            {
                "title": "Ejercicio 31",
                "desc": "Realice un algoritmo, tal que, dado el nombre de una persona (de máximo 8 caracteres), el apellido (de máximo 6), la edad en años y su peso en libras, calcule y muestre la edad en número de días, el peso en kilogramos, y el apellido seguido del nombre.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/qaxyyrofel8o16x/A31.png?dl=1"
            },
            {
                "title": "Ejercicio 32",
                "desc": "Pedro tiene el triple de la edad de Juan. Construya un algoritmo, que dada la edad de Pedro, calcule y muestre la edad de Juan.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/jdmh4vx0nvz7eg8/A32.png?dl=1"
            },
            {
                "title": "Ejercicio 33",
                "desc": "El supermercado Pepín se encuentra en promoción “miércoles de verduras y frutas”; descuento del 15% sobre las compras. Suponiendo que la suma de todas las verduras y frutas compradas fue V, construya un algoritmo que calcule y muestre cuánto habría que pagar si hoy fuera miércoles.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/0i2dxr5slxydhyg/A33.png?dl=1"
            },
            {
                "title": "Ejercicio 34",
                "desc": "Su profesor de programación ha sacado 4 notas a lo largo del semestre y es el momento de conocer su definitiva. Construya un algoritmo, que dadas unas 4 notas, calcule y muestre el promedio de las mismas.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/6kes0a3924hc3s7/A34.png?dl=1"
            },
            {
                "title": "Ejercicio 35",
                "desc": "Se necesitan 20 ladrillos para cubrir un área de 1 m2; suponiendo  que hay que cubrir un área de X metros * Y metros; construya un algoritmo que calcule y muestre cuantos ladrillos se necesitarían en total para cubrir toda el área.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/jktarx7gu5ndnw2/A35.png?dl=1"
            },
            {
                "title": "Ejercicio 36",
                "desc": "Usted saca un crédito en el banco a 5 años con un interés anual del 3%. Construya un algoritmo, que conocido el monto inicial del préstamo, calcule y muestre cuanto debe pagar al cabo de los 5 años.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/9nfzrgemfafmx99/A36.png?dl=1"
            },
            {
                "title": "Ejercicio 37",
                "desc": "Simular el lanzamiento de una moneda al aire e imprimir si ha salido cara o sello.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/989hk5eo9yt4npo/A37.png?dl=1"
            },
            {
                "title": "Ejercicio 38",
                "desc": "Escribir un programa que calcule el cociente y el residuo dados dos números enteros.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/rfbrmbsgcse80bx/A38.png?dl=1"
            },
            {
                "title": "Ejercicio 39",
                "desc": "El supermercado JARDIN se encuentra en promoción: \"Martes de Plantas Enanas; descuento del 15%\" y \"Jueves de Árboles; descuento del 30%\"  sobre las compras. Suponiendo un cliente compra 1 producto el martes y 2 productos el Jueves, muestre cuánto habría que pagar.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/rfbrmbsgcse80bx/A38.png?dl=1"
            },
            {
                "title": "Ejercicio 40",
                "desc": "Realice un algoritmo donde se calcule el sueldo de un trabajador, cuando el mismo devenga por día $45.000. Y el trabajo que realiza la persona es Lunes, Miércoles y Viernes. Nota: El trabajo es por el mes.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/rfbrmbsgcse80bx/A38.png?dl=1"
            }
        ]
    });
    page.bindingContext = vm;
}
exports.ready = ready;



function onListViewLoaded(args) {
    const listView = args.object;

}
exports.onListViewLoaded = onListViewLoaded;

function onItemTap(args) {
    const index = args.index;
    const tappedItemIndex = args.myTitles;
    const tappedItemView = args.view;
    console.log("Index: "+index);

    var info = args.view.bindingContext;
    console.log(info); // [Object object]
    console.log(info["title"]); // e.g. info["name"] === "Reg4"
    // info is Object of type { name: "Reg4" }

    var navigationEntry = {
        moduleName: "views/aprender/detalle-operadores",
        context: info
    }
    console.log("detalle algoritmo");

    const button = args.object;
    const page = button.page;
    page.frame.navigate(navigationEntry);
}
exports.onItemTap = onItemTap;

exports.regresar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/aprender-page");
}
