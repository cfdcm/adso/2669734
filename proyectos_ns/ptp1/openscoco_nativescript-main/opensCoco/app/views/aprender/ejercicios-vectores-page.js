
const { fromObject } = require("@nativescript/core/data/observable")


function ready(args) {

    const page = args.object;
    const vm = fromObject({
        // Setting the listview binding source
        myTitles: [
            {
                "title": "Ejercicio 117",
                "desc": "Hay unos multicines con 5 salas, y cada sala con 100 personas distribuidas en 20 asientos y 5 filas. Si yo pido entrada para una sala, implementar un programa que me diga si hay sitio en la sala.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 118",
                "desc": "Dada una matriz cuadrada de M*N elementos, actualizarla tal que la matriz resultante tenga divididos a los elementos de la diagonal principal por ceros y conservar los demás valores.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 119",
                "desc": "Tengo guardado en una estructura los alumnos de nuestra escuela, sabiendo que hay 3 cursos, M alumnos por curso y N asignaturas por alumno, determinar mediante funciones: \na. Cual es la nota media de un determinado curso. \nb. Cuantos aprobados( >=3.5) y suspensos ( < 3.5) hay en una determinada asignatura. \nc. Cual es el alumno de la escuela con mejor nota media.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 120",
                "desc": "Una empresa consta de 5 departamentos con 20 empleados cada departamento, si tengo todas las ventas en una estructura, determinar: \n- Ventas de un determinado departamento en un determinado mes. \n- Ventas de un determinado empleado en un determinado departamento. \n- Cual es el departamento con más ventas.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 121",
                "desc": "Hacer un programa que lea las calificaciones de un alumno en 10 asignaturas, las almacene en un vector o lista y calcule e imprima su media.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 122",
                "desc": "Hacer un programa que lea las calificaciones de un alumno en 10 asignaturas, las almacene en un vector. hacer que busque una nota en el vector.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 123",
                "desc": "Generar una matriz de 4 filas y 5 columnas con numeros aleatorios entre 1 y 100, e imprimirla.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 124",
                "desc": "Una empresa guarda en una tabla de 3x12x4 las ventas realizadas por sus tres representantes a lo largo de doce meses de sus cuatro productos, VENTAS[ representante, mes, producto ]. Mostrar el total de ventas por producto, para lo cual sumamos las ventas de cada producto de cada mes de todos los representantes. Imprimir ambos arrays.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            }
        ]
    });
    page.bindingContext = vm;
}
exports.ready = ready;



function onListViewLoaded(args) {
    const listView = args.object;

}
exports.onListViewLoaded = onListViewLoaded;

function onItemTap(args) {
    const index = args.index;
    console.log(`Second ListView item tap ${index}`);

    var info = args.view.bindingContext;
    console.log(info); // [Object object]
    console.log(info["title"]); // e.g. info["name"] === "Reg4"
    // info is Object of type { name: "Reg4" }

    var navigationEntry = {
        moduleName: "views/aprender/detalle-vectores",
        context: info
    }
    console.log("detalle algoritmo");

    const button = args.object;
    const page = button.page;
    page.frame.navigate(navigationEntry);
}
exports.onItemTap = onItemTap;

exports.regresar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/aprender-page");
}
