
const { fromObject } = require("@nativescript/core/data/observable")


function ready(args) {

    const page = args.object;
    const vm = fromObject({
        // Setting the listview binding source
        myTitles: [
            {
                "title": "Ejercicio 41",
                "desc": "Desarrolle un algoritmo que permita leer dos valores distintos, determinar cual de los dos valores es el mayor y escribirlo. ",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 42",
                "desc": "Realizar un algoritmo que permita leer dos valores, determinar cual de los dos valores es el menor y escríbalo ",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 43",
                "desc": "Desarrolle un algoritmo que permita leer tres valores y almacenarlos en las variables A, B y C respectivamente. El algoritmo debe imprimir cual es el mayor y cual es el menor. Recuerde constatar que los tres valores introducidos por el teclado sean valores distintos. Presente un mensaje de alerta en caso de que se detecte la introducción de valores iguales. ",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 44",
                "desc": "Desarrolle un algoritmo que permita leer tres valores y almacenarlos en las variables A, B, y C respectivamente. El algoritmo debe indicar cual es el menor. Asumiendo que los tres valores introducidos por el teclado son valores distintos. ",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 45",
                "desc": "Desarrolle un algoritmo que lea cuatro números diferentes y a continuación imprima el mayor de los cuatro números introducidos y también el menor de ellos. ",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 46",
                "desc": "Desarrolle un algoritmo que permita leer un valor cualquiera N y escriba si dicho número es par o impar ",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 47",
                "desc": "Desarrolle un algoritmo que permita convertir calificaciones numéricas, según la siguiente tabla: \nA = 19 y 20, B =16, 17 y 18, C = 13, 14 y 15, D = 10, 11 y 12, E = 1 hasta el 9. \nSe asume que la nota está comprendida entre 1 y 20. ",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 48",
                "desc": "Desarrolle un algoritmo que permita leer dos números y ordenarlos de menor a mayor, si es el caso. ",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 49",
                "desc": "Realice un algoritmo que calcule el monto a pagar por el servicio de estacionamiento, teniendo en cuenta que por la primera hora de estadía se tiene una tarifa de $3.500 y las restantes tienen un costo de $1.000. Se tiene como datos: hora de entrada, hora de salida (formato militar), iniciada una hora se contabiliza como hora total. NO usar minutos.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 50",
                "desc": "Realice un algoritmo que determine el pago a realizar por la entrada a un espectáculo donde se pueden comprar sólo hasta cuatro entradas, donde al costo de dos entradas se les descuenta el 10%, al de tres entradas el 15% y a la compra de cuatro tickets se le descuenta el 20 %.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 51",
                "desc": "Algoritmo que lea tres números distintos y nos diga cual de ellos es el mayor (recuerda usar la estructura condicional Si y los operadores lógicos).",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 52",
                "desc": "Realizar un algoritmo que lea un número por teclado. En caso de que ese número sea 0 o menor que 0, se saldrá del programa imprimiendo antes un mensaje de error. Si es mayor que 0, se deberá calcular su cuadrado y la raiz cuadrada del mismo, visualizando el numero que ha tecleado el usuario y su resultado (“Del numero X, su potencia es X y su raiz X” ). Para calcular la raiz cuadrada se puede usar la librería math y usar la función math.sqrt(X) o con una potencia de **0,5.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 53",
                "desc": "Una tienda ofrece un descuento del 15% sobre el total de la compra durante el mes de octubre. Dado un mes y un importe (valor de la compra), calcular cuál es la cantidad que se debe cobrar al cliente.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 54",
                "desc": "Hacer un programa que pueda dibujar una recta, un punto o un rectángulo.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 55",
                "desc": "Leer una nota y escribir en pantalla la calificación en palabras.\n1..4: escribir “suspenso” \n5..6: escribir “aprobado” \n7..8: escribir “Notable” \n9: escribir “Sobresaliente” \n10: escribir “Matricula de honor”",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 56",
                "desc": "Una compañía dedicada al alquiler de automóviles cobra un monto fijo de $300 para los primeros 300 Km. de recorrido. Para más de 300 Km. y hasta 1000 Km., cobra un monto adicional de $15 por cada kilómetro en exceso sobre 300. Para más de 1000 Km. cobra un monto adicional de $10 por cada kilómetro en exceso sobre 1000. Los precios no incluyen el 19% del impuesto al valor agregado, IVA. Diseñe un programa que determine el monto a pagar por el alquiler de un vehículo y el monto incluido del impuesto.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 57",
                "desc": "Dado cualquier par de números enteros construya un programa que reste siempre el menor del mayor.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 58",
                "desc": "Si representamos los días de la semana con dígitos tendremos que el 1 corresponde al lunes, el 2 al martes y así sucesivamente. Dado un entero cualquiera, construya un programa que muestre el nombre del día de la semana al que corresponde.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 59",
                "desc": "Construya un programa que dada una letra cualquiera indique si se trata de una vocal o no.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 60",
                "desc": "Construya un programa que redondee un número es decir, que convierta un número decimal a su equivalente entero.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 61",
                "desc": "Construya un programa que dado dos valores X y Y, verifique si las siguientes ecuaciones arrojan el mismo resultado: \na) X + 3Y \nb) 2X – 5Y  ",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 62",
                "desc": "Introducir un numero por teclado. Que nos diga si es positivo o negativo ",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 63",
                "desc": "Introducir un numero menor de 5000 y pasarlo a numero romano.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 64",
                "desc": "En el mundo del trading, existe algo que se llama trading binario. Construir un algoritmo que permita predecir si un activo subirá o bajará en el próximo minuto y que aleatoriamente se compare la respuesta del usuario con el valor del activo. El usuario antes de participar debe decir su apuesta en dólares. En caso de que la predicción esté correcta el usuario gana lo que apuesta y en caso contrario lo pierde.\nMostrar los resultados.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            }
        ]
    });
    page.bindingContext = vm;
}
exports.ready = ready;



function onListViewLoaded(args) {
    const listView = args.object;

}
exports.onListViewLoaded = onListViewLoaded;

function onItemTap(args) {
    const index = args.index;
    console.log(`Second ListView item tap ${index}`);

    var info = args.view.bindingContext;
    console.log(info); // [Object object]
    console.log(info["title"]); // e.g. info["name"] === "Reg4"
    // info is Object of type { name: "Reg4" }

    var navigationEntry = {
        moduleName: "views/aprender/detalle-condicionales",
        context: info
    }
    console.log("detalle algoritmo");

    const button = args.object;
    const page = button.page;
    page.frame.navigate(navigationEntry);
}
exports.onItemTap = onItemTap;

exports.regresar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/aprender-page");
}
