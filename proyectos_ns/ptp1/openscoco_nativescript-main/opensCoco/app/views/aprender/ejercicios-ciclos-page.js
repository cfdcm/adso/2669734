
const { fromObject } = require("@nativescript/core/data/observable")


function ready(args) {

    const page = args.object;
    const vm = fromObject({
        // Setting the listview binding source
        myTitles: [
            {
                "title": "Ejercicio 65",
                "desc": "Desarrolle un algoritmo que realice la sumatoria de los números enteros comprendidos entre el 1 y el 10, es decir, 1 + 2 + 3 + …. + 10. ",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 66",
                "desc": "Desarrolle un algoritmo que realice la sumatoria de los números enteros múltiplos de 5, comprendidos entre el 1 y el 100, es decir, 5 + 10 + 15 +…. + 100. El programa deberá imprimir los números en cuestión y finalmente su sumatoria ",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 67",
                "desc": "Desarrolle un algoritmo que realice la sumatoria de los números enteros pares comprendidos entre el 1 y el 100, es decir, 2 + 4 + 6 +…. + 100. El programa deberá imprimir los números en cuestión y finalmente su sumatoria ",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 68",
                "desc": "Desarrolle un algoritmo que lea los primeros 300 números enteros y determine cuántos de ellos son impares; al final deberá indicar su sumatoria. ",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 69",
                "desc": "Desarrolle un algoritmo que le permita determinar de una lista de números: &#10;a. ¿Cuántos están entre el 50 y 75, ambos inclusive? &#10;b. ¿Cuántos mayores de 80? &#10;c. ¿Cuántos menores de 30?&#10;El algoritmo debe finalizar cuando n (el total de números de la lista), sea igual a 0. ",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 70",
                "desc": "Desarrolle un algoritmo que permita realizar la escritura de los primeros 100 números naturales utilizando la estructura Mientras (While). ",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 71",
                "desc": "Desarrolle un algoritmo que permita leer un valor entero positivo N y determinar si es primo o no. &#10;Nota: Número primo es aquel que solo es divisible por si mismo y por la unidad",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 72",
                "desc": "Realice un algoritmo que determine los veinte primeros números, ¿Cuáles son múltiplos de 2?. ",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 73",
                "desc": "Desarrolle un algoritmo que permita calcular Promedio de N Notas; finaliza cuando N = 0. ",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 74",
                "desc": "Desarrolle un algoritmo para la empresa Constructora Gisaico., que le permita calcular e imprimir la nómina para su cancelación a un total de 50 obreros calificados a quienes debe cancelar por horas trabajadas. La hora trabajada se pautó en $30.000. ",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 75",
                "desc": "Desarrolle un algoritmo que permita determinar a partir de un número de días, ingresado por pantalla, ¿Cuántos años, meses, semanas y días; constituyen el número de días proporcionado utilizando la estructura Mientras o While. ",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 76",
                "desc": "Realice un algoritmo que calcule el factorial de un número.&#10;Nota: 5 factorial es lo mismo que, 5! = 5*4*3*2*1 = 120",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 77",
                "desc": "Se pide representar el algoritmo que nos calcule la suma de los N primeros números pares a partir de dicho numero. Es decir, si insertamos un 5, nos haga la suma de 6+8+10+12+14.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 78",
                "desc": "Dada una secuencia de números leídos por teclado, que acabe con un –1, por ejemplo: 5,3,0,2,4,4,0,0,2,3,6,0,……,-1; Realizar el algoritmo que calcule la media aritmética. Suponemos que el usuario no insertara numero negativos.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 79",
                "desc": "Calcular las calificaciones de un grupo de alumnos. La nota final de cada alumno se calcula según el siguiente criterio: la parte práctica vale el 10%; la parte de problemas vale el 50% y la parte teórica el 40%. El algoritmo leerá el nombre del alumno, las tres notas, escribirá el resultado y volverá a pedir los datos del siguiente alumno hasta que el nombre sea una cadena vacía. Las notas deben estar entre 0 y 10, si no lo están, no imprimirá las notas, mostrara un mensaje de error y volverá a pedir otro alumno.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 80",
                "desc": "Crear un algoritmo que calcule la raíz cuadrada del número que introduzca el usuario. Si se introduce un número negativo, debe mostrar un mensaje de error y volver a pedirlo (tantas veces como sea necesario).",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 81",
                "desc": "Calcular el máximo de números positivos introducidos por teclado, sabiendo que metemos números hasta que introduzcamos uno negativo. El negativo no cuenta.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 82",
                "desc": "Determinar cuales son los múltiplos de 5 comprendidos entre 1 y N.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 83",
                "desc": "Al final de curso deseamos saber cuál ha sido el alumno de primero con mejor nota media. Se sabe que este año entraron 150 alumnos y que en primero todos tienen 5 asignaturas. Dar el nombre y la nota media.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 84",
                "desc": "Calcular la suma de los divisores de cada número introducido por teclado. Terminaremos cuando el número sea negativo o 0.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 85",
                "desc": "Se coloca un capital C, a un interés I, durante M años y se desea saber en cuánto se habrá convertido ese capital en m años, sabiendo que es acumulativo.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 86",
                "desc": "Dada una fecha en formato dia/mes/año determinar el número de días y el nombre del mes de dicha fecha, y sacar por pantalla la fecha convertida a formato de: dia “de” mes “de” año.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 87",
                "desc": "Hacer un algoritmo que imprima los numeros del 1 al 100.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 88",
                "desc": "Hacer un algoritmo que imprima los numeros del 100 al 0, en orden decreciente.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 89",
                "desc": "Hacer un algoritmo que imprima los numeros pares entre 0 y 100.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 90",
                "desc": "Hacer un programa que imprima la suma de los 100 primeros numeros",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 91",
                "desc": "Hacer un algoritmo que imprima los numeros impares hasta el 100 y que imprima cuantos impares hay.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 92",
                "desc": "Hacer un algoritmo que imprima todos los numeros naturales que haydesde la unidad hasta un numero que introducimos por teclado ",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 93",
                "desc": "Introducir tantas frases como queramos y contarlas. Preguntar cada vez si quiere ingresar una frase nueva. Terminar cuando diga “NO”.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 94",
                "desc": "Hacer un algoritmo que solo nos permita introducir S o N. ",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 95",
                "desc": "Imprimir y contar los multiplos de 3 desde la unidad hasta un numero que introducimos por teclado.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 96",
                "desc": "Hacer un algoritmo que imprima los numeros del 1 al 100. Que calcule la suma de todos los numeros pares por un lado, y por otro, la de todos los impares.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 97",
                "desc": "Hacer un algoritmo que imprima el mayor y el menor de una serie de cinco numeros que vamos introduciendo por teclado. ",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 98",
                "desc": "Introducir dos numeros por teclado. Imprimir los numeros naturales que hay entre ambos numeros empezando por el más pequeño, contar cuantos hay y cuantos de ellos son pares. Calcular la suma de los impares.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 99",
                "desc": "Imprimir diez veces la serie de numeros del 1 al 10",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 100",
                "desc": "Imprimir, contar y sumar los multiplos de 2 que hay entre una serie de numeros, tal que el segundo sea mayor o igual que el primero.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 101",
                "desc": "Un usuario quiere llevar la cuenta de cuantas personas entran a su restaurante hasta las 6pm. Asignarle una mesa disponible de las 20 que hay en el establecimiento a cada persona que entre y controlar disponibilidad. Nota: cada 5 personas que entran se libera 1 mesa.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 102",
                "desc": "Hacer un algoritmo que simule el funcionamiento de un reloj digital militar.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 103",
                "desc": "Introducir una frase por teclado. Imprimirla cinco veces en filas consecutivas, pero cada impresion ir desplazada cuatro columnas hacia la derecha. ",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 104",
                "desc": "Realizar la tabla de multiplicar de un numero entre 0 y 10.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 105",
                "desc": "Simular cien tiradas de dos dados y contar las veces que entre los dos suman 10.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 106",
                "desc": "Hacer un programa que nos permita introducir un numero por teclado y sobre el se realicen las siguientes operaciones: comprobar si es primo, hallar su factorial o imprimir su tabla de multiplicar.",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 107",
                "desc": "Construir una calculadora básica: suma, resta, multiplicación, div. Nota: Controla división por cero.&#10;Preguntar al usuario que desea hacer cada vez hasta que digite la opción \"salir\". Use un menu y use estructura según (switch).",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            }
        ]
    });
    page.bindingContext = vm;
}
exports.ready = ready;



function onListViewLoaded(args) {
    const listView = args.object;
    
}
exports.onListViewLoaded = onListViewLoaded;

function onItemTap(args) {
    const index = args.index;
    console.log(`Second ListView item tap ${index}`);

    var info = args.view.bindingContext;
    console.log(info); // [Object object]
    console.log(info["title"]); // e.g. info["name"] === "Reg4"
    // info is Object of type { name: "Reg4" }

    var navigationEntry = {
        moduleName: "views/aprender/detalle-ciclos",
        context: info
    }
    console.log("detalle algoritmo");
    
    const button = args.object;
    const page = button.page;
    page.frame.navigate(navigationEntry);
}
exports.onItemTap = onItemTap;

exports.regresar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/aprender-page");
}