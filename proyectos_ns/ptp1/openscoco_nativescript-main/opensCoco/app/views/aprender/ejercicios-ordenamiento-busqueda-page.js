
const { fromObject } = require("@nativescript/core/data/observable")


function ready(args) {

    const page = args.object;
    const vm = fromObject({
        // Setting the listview binding source
        myTitles: [
            {
                "title": "Ejercicio 125",
                "desc": "Método de la burbuja",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 126",
                "desc": "Método de inserción",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 127",
                "desc": "Método de la selección",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 128",
                "desc": "Método de ordenación rápida o QuickShort",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 129",
                "desc": "Búsqueda secuencial",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 130",
                "desc": "Búsqueda secuencial con centinela",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 131",
                "desc": "Búsqueda binaria o dicotónica",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 132",
                "desc": "Búsqueda por transformación de claves o Hashing",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            }
        ]
    });
    page.bindingContext = vm;
}
exports.ready = ready;



function onListViewLoaded(args) {
    const listView = args.object;
    
}
exports.onListViewLoaded = onListViewLoaded;

function onItemTap(args) {
    const index = args.index;
    console.log(`Second ListView item tap ${index}`);

    var info = args.view.bindingContext;
    console.log(info); // [Object object]
    console.log(info["title"]); // e.g. info["name"] === "Reg4"
    // info is Object of type { name: "Reg4" }

    var navigationEntry = {
        moduleName: "views/aprender/detalle-ord_bus",
        context: info
    }
    console.log("detalle algoritmo");
    
    const button = args.object;
    const page = button.page;
    page.frame.navigate(navigationEntry);
}
exports.onItemTap = onItemTap;

exports.regresar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/aprender-page");
}