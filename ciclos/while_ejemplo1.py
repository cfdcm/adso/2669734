# Ciclos, loop, bucle, vueltas, repeticiones, iteraciones, pasadas
# Pedir 7 edades y sumarlas. Mostrar el resultado. Calcular el promedio.

contador = 1
acumulador = 0

while contador <= 7:
    edad = int(input(f"Digite la edad {contador}: "))
    acumulador = acumulador + edad      # Acumulador
    contador = contador + 1     # Contador

print(f"La suma de las edades es: {acumulador}")
