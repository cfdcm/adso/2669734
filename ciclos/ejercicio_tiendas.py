import random
productos = ["Leche", "Huevos", "Arepas", "Queso", "Plátanos", "Arroz", "Papa", "Pescado"]

comprados = [0, 0, 0, 0, 0, 0, 0, 0]

cont = 1
while cont <= 6:
    indice = 0
    for p in productos:
        if comprados[indice] == 0:
            print(f"Tienda {cont}, tiene el producto {p}?: ", end="")
            respuesta = random.randint(0, 1)
            if respuesta == 1:
                comprados[indice] = 1
                print("SI")
            else:
                print("NO")

        indice += 1

    if 0 not in comprados:
        print("="*50)
        print("*** Todos los productos conseguidos...")
        break
    else:
        # print(f"La compra va así: {comprados}")
        if cont < 6:
            pausa = input("[Enter] para continuar: ")

    cont += 1

print("================= Informe final ==================")

indice = 0
for c in comprados:
    if c == 1:
        print(f"+++ Producto comprado {productos[indice]}")
    else:
        print(f"--- Producto NO conseguido {productos[indice]}")
    indice += 1
