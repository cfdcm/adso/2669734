# Construir un algoritmo con for. Cuenta regresiva, para el factorial de N! (pedido por pantalla).
# Ejemplo, factorial 5! es: 5x4x3x2x1 = 120
# Factorial de 0 = 1

n = int(input("Digite un num para calcula su factorial: "))
mult = 1
for i in range(n, 0, -1):
    if i == n:
        print(f"{n}! es {i}", end="")
    else:
        print(f"x{i}", end="")
    mult *= i
print(f" = {mult}")
