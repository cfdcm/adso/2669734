# Ciclo for. Generador de números. Función range()

# Pida 3 edades (Llenar el vector) y calcule el promedio.

edades = []
suma = 0
for i in range(3):  # [0,1,2]
    e = int(input(f"Digite la edad {i+1}: "))
    suma += e
    edades.append(e)

print(f"Vector: {edades}")
print(f"La i del ciclo terminó en: {i+1}")
print(f"Promedio: {suma/(i+1)}")
