# Preguntar al usuario números hasta que diga "salir".
# Mostrar la sumatoria de ellos.

dato = ""
suma = 0
while dato != "salir":
    dato = input("Digite un num o 'salir' para terminar: ")
    if dato != "salir":
        suma += int(dato)


print(f"El total fue: {suma}")
