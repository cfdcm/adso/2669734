l_d1 = []
l_d2 = []

import random

for i in range(10):
    dado1 = random.randint(1, 6)   # Azar entre 1 y 6. Números enteros
    l_d1.append(dado1)
    dado2 = random.randint(1, 6)   # Azar entre 1 y 6. Números enteros
    l_d2.append(dado2)

print(l_d1)
print(l_d2)

# sumar los valores de los dos dados, sólo cuando se saque doble 1, o doble 6. Mostrar valor.

suma = 0
for i in range(10):
    print(f"d1: {l_d1[i]} - d2: {l_d2[i]}")
    if (l_d1[i] == 1 and l_d2[i] == 1) or (l_d1[i] == 6 and l_d2[i] == 6):
        suma = l_d1[i] + l_d2[i]
        print(f"Caso correcto, la suma es: {suma}")

