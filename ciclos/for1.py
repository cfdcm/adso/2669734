# Calcular el promedio de las edades de la lista.
"""
edades = [20, 38, 40, 25]

cont = 0
suma = 0
for edad in edades:
    suma += edad        # Acumulador
    cont += 1           # Contador

print(f"El promedio es: {suma/cont}")
"""

# Mismo ejercicio llenando el vector.
edades = []
edades.append(input("Digite su nombre"))
edades.append(int(input("Digite su edad")))

num = int(input("Digite un valor"))
edades.append(num)

print(edades)
