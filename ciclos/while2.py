# Sumar los primeros 50 números naturales, sin incluir el 10, 20, 30 y el 40.
c = 1
total = 0
while c <= 50:

    if c != 10 and c != 20 and c != 30 and c != 40:
        total += c  # Acumulador

    print(c)
    c += 1

print(f"El total es: {total}")
