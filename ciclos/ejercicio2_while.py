# pedir el número del mes (1-12) y no dejar pasar hasta que esté correcto.
# imprimir mes.
"""
while True:
    mes = int(input("Digite un mes entre 1-12"))
    if mes < 1 or mes > 12:
        print("Error, mes incorrecto...")
    else:
        break

print(f"Muy bien, mes: {mes}")

"""

mes = 0
while mes < 1 or mes > 12:
    mes = int(input("Digite un mes entre 1-12: "))

print(f"Muy bien, mes: {mes}")
