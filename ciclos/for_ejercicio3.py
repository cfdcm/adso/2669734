# Pedir los nombres y las notas de 3 aprendices. (Llenar los vectores)
# Averiguar cuál aprendiz tiene mejor nota y mostrar nombre y nota.

notas = [2, 5, 4.8, 3]

mejor = -1
peor = 999
for i in notas:
    if i > mejor:
        mejor = i

    if i < peor:
        peor = i

print(mejor)
print(peor)
