# Ciclo for. Bucle, secuencia, iteraciones, repetidor, loop, vueltas, pasadas.

# Cadenas de caracteres. "Hola mundo!!!"

for i in "Hola mundo mundia!!!":
    if i == "m":
        print("Si hay 'm' en la cadena...")

# Vectores, Array, Arreglo o Listas (Python), listas unidimensionales

v1 = [5, "hola", True, 4.8, "Manzana"]

for i in v1:
    print(i)

frutas = ["Manzana", "Fresa", "Uva"]

for fruta in frutas:
    print(fruta)


v2 = [-1, 0, 1, 2, 3, 4, 5]

for i in v2:
    print(i)
