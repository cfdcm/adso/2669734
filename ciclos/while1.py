# Ciclo mientras o while, completo.

cont = 0        # Contador
suma = 0        # Acumulador
while True:
    cont += 1       # Contador
    suma += cont    # Acumulador
    print(cont)
    print(suma)
    if cont == 80:
        break           # Rompe el ciclo, o termina el ciclo


