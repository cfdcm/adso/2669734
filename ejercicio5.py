# Construya un algoritmo que calcule el promedio de tres notas de algoritmos.

nota1 = float( input("Digite el nota1: ") )
nota2 = float( input("Digite el nota2: ") )
nota3 = float( input("Digite el nota3: ") )

promedio = (nota1 + nota2 + nota3) / 3

print(f"El promedio es: {promedio:0.4f}")
