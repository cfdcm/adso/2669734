# Variables

# Tipos de datos
"""
							Python
"hola"		string	cadena		texto		str
5		integer	entero		números		int
3.8		float	decimal		fraccionario	float
True|False	bool	boleanos 1|0	Verdadero|Falso	bool
Null		Null	Nulos		no conocidos	None
"2023/02/07"	Date	Fecha		Fecha		date
"""

numero1 = 5		# inicialización de la variable
sueldo = ""		# iniciar la variable

print(numero1)

numero1 = 7		# definición de la variable
apellido = "Pérez"	# definir la variable

print(numero1)

