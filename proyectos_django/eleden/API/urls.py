from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import *

router = DefaultRouter()
router.register(r'peliculas', PeliculaViewSet)

app_name = 'API'

urlpatterns = [
    path('', index, name='index'),
    path("api/1.0/", include(router.urls)),
]
