from .models import *
from rest_framework import serializers


class PeliculaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Pelicula
		# fields = ['id', 'titulo', 'imagen', 'estreno', 'resumen']
		fields = '__all__'

