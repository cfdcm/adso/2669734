from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from .models import Pelicula
from .serializers import PeliculaSerializer
from rest_framework import viewsets


def index(request):
	return HttpResponse("Bienvenido a Django-rest")


class PeliculaViewSet(viewsets.ModelViewSet):
	queryset = Pelicula.objects.all()
	serializer_class = PeliculaSerializer
