function confirmar_eliminar(ruta){
    if(confirm("Está seguro?")){
        location.href = ruta;
    }
    else{
        alert("Fiuu, te salvaste...")
    }
}


function ver_carrito(ruta){
    // data: { name: "John", location: "Boston" }
    r = $("#respuesta_carrito")

    $.ajax({
        method: "GET",
        url: ruta
    })
    .done(function( respuesta ) {
        r.html(respuesta)
    })
    .fail(function() {
        alert( "error" );
    });
}

function add_carrito(ruta, id){
    r = $("#respuesta_carrito")

    id_producto = $("#id_producto_"+id).val()
    cantidad = $("#cantidad_"+id).val()

    $.ajax({
        method: "GET",
        url: ruta,
        data: { "id_producto": id_producto, "cantidad": cantidad },
        dataType: 'json'
    })
    .done(function( respuesta ) {
        console.info(JSON.stringify(respuesta));
        respuesta_html = ""
        tabla = document.createElement("table");
        tabla.setAttribute("class", "table");

        thead = document.createElement("thead");

        tr1 = document.createElement("tr");
        th1 = document.createElement("th");
        titul1 = document.createTextNode("ID");
        th2 = document.createElement("th");
        titul2 = document.createTextNode("Cantidad");

        th1.appendChild(titul1);
        th2.appendChild(titul2);
        tr1.appendChild(th1);
        tr1.appendChild(th2);

        thead.appendChild(tr1);
        tabla.appendChild(thead);

        tbody = document.createElement("tbody");
        for(i = 0; i< respuesta.carrito.length; i++){
            tr = document.createElement("tr")
            td1 = document.createElement("td")
            td2 = document.createElement("td")
            t1 = document.createTextNode(respuesta.carrito[i].id);
            t2 = document.createTextNode(respuesta.carrito[i].cantidad);

            td1.appendChild(t1)
            td2.appendChild(t2)
            tr.appendChild(td1)
            tr.appendChild(td2)

            tbody.appendChild(tr)
        }
        tabla.appendChild(tbody)
        r.html(tabla);

        // abrir offcanvas
        offcanvas = $("#offcanvasRight").offcanvas('toggle');
    })
    .fail(function() {
        alert( "error" );
    });
}