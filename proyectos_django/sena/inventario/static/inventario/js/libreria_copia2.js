function confirmar_eliminar(ruta){
    if(confirm("Está seguro?")){
        location.href = ruta;
    }
    else{
        alert("Fiuu, te salvaste...")
    }
}


function ver_carrito(ruta){
    // data: { name: "John", location: "Boston" }
    r = $("#respuesta_carrito")

    $.ajax({
        method: "GET",
        url: ruta
    })
    .done(function( respuesta ) {
        r.html(respuesta)
    })
    .fail(function() {
        alert( "error" );
    });
}

function add_carrito(ruta, id){
    r = $("#respuesta_carrito")

    id_producto = $("#id_producto_"+id).val()
    cantidad = $("#cantidad_"+id).val()

    $.ajax({
        method: "GET",
        url: ruta,
        data: { "id_producto": id_producto, "cantidad": cantidad },
        dataType: 'json'
    })
    .done(function( respuesta ) {
        //console.info(JSON.stringify(respuesta));
        respuesta_html = ""
        respuesta_html += "<table class='table'>"
        respuesta_html += "<thead>"
        respuesta_html += "<tr>"
        respuesta_html += "<th>ID</th><th>Cantidad</th>"
        respuesta_html += "</tr>"
        respuesta_html += "</thead>"
        respuesta_html += "<tbody>"
        for(i = 0; i< respuesta.carrito.length; i++){
            respuesta_html += "<tr>"
            respuesta_html += "<td>"+respuesta.carrito[i].id+"</td>"
            respuesta_html += "<td>"+respuesta.carrito[i].cantidad+"</td>"
            respuesta_html += "</tr>"
        }
        respuesta_html += "</tbody>"
        respuesta_html += "</table>"
        r.html(respuesta_html);

        // abrir offcanvas
        offcanvas = $("#offcanvasRight").offcanvas('toggle');
    })
    .fail(function() {
        alert( "error" );
    });
}