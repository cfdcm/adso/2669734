function confirmar_eliminar(ruta){
    if(confirm("Está seguro?")){
        location.href = ruta;
    }
    else{
        alert("Fiuu, te salvaste...")
    }
}


function ver_carrito(ruta){
    // data: { name: "John", location: "Boston" }
    r = $("#offcanvasRight")

    $.ajax({
        method: "GET",
        url: ruta
    })
    .done(function( respuesta ) {
        r.html(respuesta)
        quitar_alertas();
    })
    .fail(function() {
        alert( "error" );
    });
}

function add_carrito(ruta, id){
    r = $("#offcanvasRight")

    id_producto = $("#id_producto_"+id).val()
    cantidad = $("#cantidad_"+id).val()

    //dataType: 'json'
    $.ajax({
        method: "GET",
        url: ruta,
        data: { "id_producto": id_producto, "cantidad": cantidad }
    })
    .done(function( respuesta ) {
        r.html(respuesta);
        // abrir offcanvas
        offcanvas = $("#offcanvasRight").offcanvas('toggle');
        quitar_alertas();
    })
    .fail(function() {
        alert( "error" );
    });
}

function del_item_carrito(ruta){
    r = $("#offcanvasRight")

    $.ajax({
        method: "GET",
        url: ruta
    })
    .done(function( respuesta ) {
        r.html(respuesta);
        quitar_alertas();
        // abrir offcanvas
        // offcanvas = $("#offcanvasRight").offcanvas('toggle');
    })
    .fail(function() {
        alert( "error" );
    });
}

function quitar_alertas(){
    console.log( "Quitando alerta..." );
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 1000);
}
