from random import randint

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from django.urls import reverse

from .models import *

from django.db import IntegrityError, transaction

from .serializers import *
from rest_framework import viewsets

from django.db.models import Q, Count
from django.conf import settings
from .encriptar import *

# Create your views here.
# ---- Agregue esto para generar Token a nuevos usuarios	*******
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
	if created:
		Token.objects.create(user=instance)
# ---- Agregue esto para generar Token a nuevos usuarios	*******


def index(request):
	logueo = request.session.get("logueo", False)
	if logueo:
		return redirect("inicio")
	else:
		return render(request, "inventario/login/login.html")


def login(request):
	if request.method == "POST":
		nick = request.POST.get("nick")
		clave = request.POST.get("clave")
		try:
			q = Usuario.objects.get(nick=nick)
			verify = verify_password(clave, q.password)
			if verify:
				messages.success(request, f"Bienvenido {q.nombre}")
				# Crear la sesión......
				request.session["logueo"] = {
					"id": q.id,
					"nombre": q.nombre,
					"rol": q.rol
				}
				request.session["carrito"] = []
				request.session["items_carrito"] = 0

				return redirect("inicio")
			else:
				raise Exception(verify)
		except Exception as e:
			messages.error(request, f"Usuario o contraseña no válidos...")
			return redirect("index")
	else:
		messages.warning(request, "No se enviaron datos...")
		return redirect("index")

def logout(request):
	logueo = request.session.get("logueo", False)
	if logueo:
		del request.session["logueo"]
		del request.session["carrito"]
		del request.session["items_carrito"]
		return redirect("index")
	else:
		messages.info(request, "No se pudo cerrar sesión, intente de nuevo")
		return redirect("inicio")


def handle_uploaded_file(f):
	with open(f"{settings.MEDIA_ROOT}/fotos/{f.name}", "wb+") as destination:
		for chunk in f.chunks():
			destination.write(chunk)


def registrarse(request):
	if request.method == "POST":
		if request.POST.get("clave1") == request.POST.get("clave2"):
			foto = request.FILES.get("foto")
			if foto is not None:
				handle_uploaded_file(foto)
				foto = f"fotos/{foto.name}"
			else:
				foto = "fotos/default.png"

			clave = hash_password(request.POST.get("clave1"))
			q = Usuario(
				nombre=request.POST.get("nombre"),
				apellido=request.POST.get("apellido"),
				nick=request.POST.get("nick"),
				clave=clave,
				foto=foto
			)
			q.save()
			messages.success(request, "Registro correcto!!")
			# logueo automático....
			request.session["logueo"] = {
				"id": q.id,
				"nombre": q.nombre,
				"rol": q.rol
			}
			return redirect("inicio")
		else:
			messages.warning(request, "No coinciden las contraseñas...")
			return redirect("index")
	else:
		return render(request, "inventario/login/registrarse.html")


def recuperar_clave_form(request):
	if request.method == "POST":
		try:
			q = Usuario.objects.get(correo=request.POST.get("correo"))
			num = randint(100000, 999999)
			# convertir num a base64 para ocultarlo un poco
			ofuscado = base64.b64encode(str(num).encode("ascii")).decode("ascii")
			q.token = ofuscado
			q.save()

			# envío de correo
			ruta = reverse(verificar_token_form, args=(q.correo,))

			resultado = enviar_correo(ruta, q.correo, q.token)
			messages.info(request, resultado)
			return redirect("verificar_token_form", correo=q.correo)
		except Usuario.DoesNotExist:
			messages.error(request, "Usuario no existe...")

		return redirect("recuperar_clave_form")
	else:
		return render(request, "inventario/login/recuperar_clave_form.html")


def verificar_token_form(request, correo):
	if request.method == "POST":
		try:
			q = Usuario.objects.get(correo=correo)
			if q.token != "" and q.token == request.POST.get("token"):
				messages.success(request, "Token OK, cambie su clave!!")
				return redirect("olvide_mi_clave", correo=correo)
			else:
				messages.error(request, "Token no válido...")
		except Usuario.DoesNotExist:
			messages.error(request, "Usuario no existe...")

		return redirect("verificar_token_form", correo=correo)
	else:
		contexto = { "correo": correo }
		return render(request, "inventario/login/verificar_token_form.html", contexto)


def olvide_mi_clave(request, correo):
	if request.method == "POST":
		c_nueva1 = request.POST.get("nueva1")
		c_nueva2 = request.POST.get("nueva2")

		q = Usuario.objects.get(correo=correo)

		if c_nueva1 == c_nueva2:
			# modifico clave al usuario actual (al objeto)
			clave = hash_password(c_nueva1)
			q.clave = clave
			# eliminar el token de db
			q.token = ""
			# guardo en base de datos
			q.save()

			messages.success(request, "Clave cambiada correctamente!!")
			return redirect("index")
		else:
			messages.warning(request, "Claves nuevas no concuerdan...")

		return redirect("olvide_mi_clave", correo=correo)
	else:
		contexto = {"correo": correo}
		return render(request, "inventario/login/olvide_mi_clave.html", contexto)


def inicio(request):
	logueo = request.session.get("logueo", False)
	if logueo:
		# Filtro de categoría por parámetro por URL
		filtro_categoria = request.GET.get("cat")
		if filtro_categoria == None:
			p = Producto.objects.all()
		else:
			p = Producto.objects.filter(categorias_id = filtro_categoria)

		c = Categoria.objects.all()

		cantidades = Producto.objects.raw("select ic.id, ic.nombre_cat, count(ip.id) as cantidad from inventario_categoria ic left join inventario_producto ip on ic.id = ip.categorias_id GROUP by ic.id")

		contexto = {
			"data": p,
			"categorias": c,
			"cantidad_productos": len(p),
			"cantidades_por_categoria": cantidades
		}
		return render(request, "inventario/index.html", contexto)
	else:
		return redirect("index")


def categorias(request):
	q = Categoria.objects.all()
	context = {"data": q}
	return render(request, "inventario/categorias/categorias_listar.html", context)


def categorias_formulario(request):
	return render(request, "inventario/categorias/categorias_formulario.html")


def categorias_guardar(request):
	if request.method == "POST":
		nombre_cat = request.POST.get("nombre_cat")
		desc = request.POST.get("desc")
		try:
			q = Categoria(nombre_cat=nombre_cat, desc=desc)
			q.save()
			messages.success(request, "Registro guardado correctamente!!")
		except Exception as e:
			messages.error(request, f"Error: {e}")

		return redirect("categorias")
	else:
		messages.warning(request, "No se enviaron datos...")
		return redirect("categorias_formulario")


def categorias_eliminar(request, id_categoria):
	try:
		q = Categoria.objects.get(pk = id_categoria)
		q.delete()
		messages.success(request, "Registro eliminado correctamente!!")
	except Exception as e:
		messages.error(request, f"Error: {e}")

	return redirect("categorias")


def categorias_formulario_editar(request, id_categoria):
	q = Categoria.objects.get(pk=id_categoria)
	datos = {"registro": q}
	return render(request, "inventario/categorias/categorias_formulario.html", datos)

def categorias_actualizar(request):
	c = Categoria.objects.get(pk=request.POST.get("id"))
	try:
		c.nombre_cat = request.POST.get("nombre_cat")
		c.desc = request.POST.get("desc")
		c.save()
		messages.success(request, "Registro actualizado correctamente!!")
	except Exception as e:
		messages.error(request, f"Error: {e}")

	return redirect("categorias")



def productos(request):
	logueo = request.session.get("logueo", False)

	if logueo and (logueo["rol"] == "SECRE" or logueo["rol"] == "ADMIN"):
		q = Producto.objects.all()
		context = {"data": q}
		return render(request, "inventario/productos/productos_listar.html", context)
	else:
		messages.info(request, "No tiene permisos para acceder al módulo...")
		return redirect("index")

def productos_formulario(request):
	q = Categoria.objects.all()
	context = {"categorias": q}
	return render(request, "inventario/productos/productos_formulario.html", context)


def productos_guardar(request):
	if request.method == "POST":
		nombre = request.POST.get("nombre")
		stock = request.POST.get("stock")
		precio = request.POST.get("precio")
		# Para llaves foráneas convertir dato en instancia de...
		categorias = Categoria.objects.get(pk=request.POST.get("categorias"))
		try:
			q = Producto(nombre=nombre, stock=stock, precio=precio, categorias=categorias)
			q.save()
			messages.success(request, "Registro guardado correctamente!!")
		except Exception as e:
			messages.error(request, f"Error: {e}")

		return redirect("productos")
	else:
		messages.warning(request, "No se enviaron datos...")
		return redirect("productos_formulario")


def productos_eliminar(request, id_producto):
	try:
		q = Producto.objects.get(pk = id_producto)
		q.delete()
		messages.success(request, "Registro eliminado correctamente!!")
	except Exception as e:
		messages.error(request, f"Error: {e}")

	return redirect("productos")


def productos_formulario_editar(request, id_producto):
	query_productos = Producto.objects.get(pk=id_producto)
	query_categorias = Categoria.objects.all()

	contexto = {"registro": query_productos, "categorias": query_categorias}
	return render(request, "inventario/productos/productos_formulario.html", contexto)

def productos_actualizar(request):
	p = Producto.objects.get(pk=request.POST.get("id"))
	try:
		p.nombre = request.POST.get("nombre")
		p.stock = request.POST.get("stock")
		p.precio = request.POST.get("precio")
		# Para llaves foráneas convertir dato en instancia de...
		p.categorias = Categoria.objects.get(pk=request.POST.get("categorias"))
		p.save()
		messages.success(request, "Registro actualizado correctamente!!")
	except Exception as e:
		messages.error(request, f"Error: {e}")

	return redirect("productos")


def buscar(request):
	if request.method == "POST":
		dato = request.POST.get("dato_buscar")
		q = Categoria.objects.filter(Q(nombre_cat__icontains=dato) | Q(desc__icontains=dato))
		contexto = {"data": q}
		return render(request, "inventario/categorias/categorias_listar.html", contexto)
	else:
		messages.warning(request, "No se enviaron datos...")
		return redirect("categorias")


def ver_perfil(request):
	logueo = request.session.get("logueo", False)

	if logueo:
		q = Usuario.objects.get(pk=logueo["id"])
		context = {"data": q}
		return render(request, "inventario/login/perfil.html", context)
	else:
		messages.info(request, "No tiene permisos para acceder al módulo...")
		return redirect("index")


def formulario_cambiar_clave(request):
	logueo = request.session.get("logueo", False)

	if logueo:
		return render(request, "inventario/login/formulario_cambiar_clave.html")
	else:
		messages.info(request, "No tiene permisos para acceder al módulo...")
		return redirect("index")


def cambiar_clave(request):
	if request.method == "POST":
		# capturo la clave actual del formulario
		c_actual = request.POST.get("actual")
		c_nueva1 = request.POST.get("nueva1")
		c_nueva2 = request.POST.get("nueva2")

		# capturo variable de sesión para averiguar ID de usuario
		logueo = request.session.get("logueo", False)
		q = Usuario.objects.get(pk=logueo["id"])
		verify = verify_password(c_actual, q.clave)
		if verify:
			if c_nueva1 == c_nueva2:
				# modifico clave al usuario actual (al objeto)
				clave = hash_password(c_nueva1)
				q.clave = clave
				# guardo en base de datos
				q.save()
				messages.success(request, "Clave cambiada correctamente!!")
			else:
				messages.warning(request, "Claves nuevas no concuerdan...")
		else:
			messages.error(request, "Clave actual no corresponde...")

		return redirect("formulario_cambiar_clave")
	else:
		return HttpResponse("No se enviaron datos...")


def carrito_add(request):
	if request.method == "GET":
		id_producto = request.GET.get("id_producto")
		cantidad = int(request.GET.get("cantidad"))
		q = Producto.objects.get(pk=id_producto)
		carrito = request.session.get("carrito", False)
		if not carrito:
			# si no existe la variable de sesión, la creamos
			request.session["carrito"] = []
			request.session["items_carrito"] = 0
			carrito = []
		# revisar el carrito para saber si agregamos o aumentamos la cantidad
		for p in carrito:
			if id_producto == p["id"]:
				p["cantidad"] += cantidad
				messages.info(request, "Producto ya existe, cantidad actualizada")
				break
		else:
			carrito.append({"id": id_producto, "cantidad": cantidad})
			messages.success(request, "Producto agregado correctamente!!")
		# actualizamos variable de sesión
		request.session["carrito"] = carrito
		request.session["items_carrito"] = len(carrito)

		"""from django.http import JsonResponse
		response = JsonResponse({"carrito": carrito})
		return response"""
		return redirect("ver_carrito")
	else:
		return HttpResponse("No se enviaron datos...")


def ver_carrito(request):
	carrito = request.session.get("carrito", False)

	total = 0
	productos = []
	for p in carrito:
		q = Producto.objects.get(pk=p["id"])
		q.cantidad = p["cantidad"]
		q.subtotal = int(p["cantidad"]) * q.precio
		productos.append(q)
		total += int(q.subtotal)

	contexto = {"data": productos, "total": total}
	return render(request, "inventario/carrito/listar_carrito.html", contexto)


def eliminar_item_carrito(request, id_producto):

	carrito = request.session.get("carrito", False)
	for i, p in enumerate(carrito):
		if id_producto == int(p["id"]):
			carrito.pop(i)
			messages.info(request, "Producto eliminado...")
			break
	else:
		messages.warning(request, "Producto no encontrado...")

	request.session["carrito"] = carrito
	request.session["items_carrito"] = len(carrito)
	return redirect("ver_carrito")


def vaciar_carrito(request):
	carrito = request.session.get("carrito", False)
	try:
		# vaciar lista....
		carrito.clear()

		request.session["carrito"] = carrito
		request.session["items_carrito"] = 0
		messages.success(request, "Ya no hay items en el carrito!!")
	except Exception as e:
		messages.error(request, "Ocurrió un error, intente de nuevo...")

	return redirect("inicio")


@transaction.atomic
def guardar_compra(request):
	carrito = request.session.get("carrito", False)

	# capturo variable de sesión para averiguar ID de usuario
	logueo = request.session.get("logueo", False)
	u = Usuario.objects.get(pk=logueo["id"])
	# import traceback
	try:
		c = Compra(usuario=u)
		c.save()
		print(f"Id compra: {c.id}")

		for p in carrito:
			try:
				pro = Producto.objects.get(pk=p["id"])
			except Producto.DoesNotExist:
				raise Exception(f"No se pudo realizar la compra, El producto {pro} ya no existe..")

			if pro.stock >= p["cantidad"]:
				dc = DetalleCompra(
					id_compra = c,
					producto = pro,
					cantidad = int(p["cantidad"]),
					precio = pro.precio
				)
				dc.save()
				# disminuir inventario
				pro.stock -= int(p["cantidad"])
				pro.save()
			else:
				raise Exception(f"El producto {pro} no tiene suficiente inventario...")

		# vaciar carrito e items en cero.
		request.session["carrito"] = []
		request.session["items_carrito"] = 0

		messages.success(request, "Compra guardada correctamente!!")
	except Exception as e:
		# ************* si ERROR **********
		transaction.set_rollback(True)
		# rollback
		messages.warning(request, f"Error: {e}")	#  - {traceback.format_exc()}

	return redirect("inicio")


def enviar_correo(ruta, correo, token):
	from django.core.mail import BadHeaderError, EmailMessage
	destinatario = correo
	mensaje = f"""
			<h1 style='color:blue;'>Inventario</h1>
			<p>Usted realizó una solicitud de cambio de clave.</p>
			<p>Haga click aquí:</p>
			<br>
			<a href='http://127.0.0.1:8000/{ruta}'>Recuperar contraseña </a>
			<br>
			<p>Digite el siguiente token: <strong>{token}</strong></p>
			"""
	print(mensaje)
	try:
		msg = EmailMessage("Tienda ADSO", mensaje, settings.EMAIL_HOST_USER, [destinatario])
		msg.content_subtype = "html"  # Habilitar contenido html
		msg.send()
		return "Correo enviado"
	except BadHeaderError:
		return "Encabezado no válido"
	except Exception as e:
		return f"Error: {e}"

# ------------------------------- Para los permisos de los end-points -----------------------------
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated

# Vistas para las APIs

class CategoriaViewSet(viewsets.ModelViewSet):
	authentication_classes = [TokenAuthentication]
	permission_classes = [IsAuthenticated]
	queryset = Categoria.objects.all()
	serializer_class = CategoriaSerializer


class ProductoViewSet(viewsets.ModelViewSet):
	permission_classes = [IsAuthenticated]
	queryset = Producto.objects.all()
	serializer_class = ProductoSerializer


class UsuarioViewSet(viewsets.ModelViewSet):
	# authentication_classes = [TokenAuthentication, SessionAuthentication]
	authentication_classes = [TokenAuthentication]
	permission_classes = [IsAuthenticated]

	queryset = Usuario.objects.all()
	serializer_class = UsuarioSerializer


from rest_framework import generics


class ProductosFiltroCategoria(generics.ListAPIView):
	serializer_class = ProductoSerializer

	def get_queryset(self):
		"""
		Vista para mostrar todos los productos de una categoría específico.
		"""
		cat = self.kwargs['categorias']
		# tema_obj = Tema.objects.get(pk=tema)
		return Producto.objects.filter(categorias__id=cat).order_by('-nombre')


# ------------------------------- Personalización de Token Autenticación ------------
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response


class CustomAuthToken(ObtainAuthToken):
	def post(self, request, *args, **kwargs):
		serializer = self.serializer_class(data=request.data,
										   context={'request': request})
		serializer.is_valid(raise_exception=True)
		user = serializer.validated_data['username']
		# traer datos del usuario para bienvenida y ROL
		usuario = Usuario.objects.get(nick=user)
		token, created = Token.objects.get_or_create(user=usuario)

		return Response({
			'token': token.key,
			'user': {
				'user_id': usuario.pk,
				'email': usuario.email,
				'nombre': usuario.nombre,
				'apellido': usuario.apellido,
				'rol': usuario.rol,
				'foto': usuario.foto.url
			}
		})
