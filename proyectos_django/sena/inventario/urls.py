from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework.documentation import include_docs_urls
# from rest_framework.authtoken.views import obtain_auth_token

from . import views

router = DefaultRouter()
router.register(r'categoria', views.CategoriaViewSet)
router.register(r'producto', views.ProductoViewSet)
router.register(r'usuario', views.UsuarioViewSet)

urlpatterns = [
	# URLs para las api del proyecto
	path("api/1.0/", include(router.urls)),
	path('api/1.0/producto_filtro_cat/<int:categorias>/', views.ProductosFiltroCategoria.as_view()),
	# Ruta para login y obtener token desde app móvil
	path('api/1.0/token-auth/', views.CustomAuthToken.as_view()),
	# Ruta para login en API web
	path('api/1.0/auth/', include("rest_framework.urls")),

	path('api/1.0/docs/', include_docs_urls(title='Inventario API')),

	# Fin URLs
	path("", views.index, name="index"),

	path("login/", views.login, name="login"),
	path("logout/", views.logout, name="logout"),
	path("registrarse/", views.registrarse, name="registrarse"),
	path("recuperar_clave_form/", views.recuperar_clave_form, name="recuperar_clave_form"),
	path("verificar_token_form/<str:correo>/", views.verificar_token_form, name="verificar_token_form"),
	path("olvide_mi_clave/<str:correo>/", views.olvide_mi_clave, name="olvide_mi_clave"),

	path("inicio/", views.inicio, name="inicio"),

	path("categorias/", views.categorias, name="categorias"),
	path("categorias_formulario/", views.categorias_formulario, name="categorias_formulario"),
	path("categorias_guardar/", views.categorias_guardar, name="categorias_guardar"),
	path("categorias_eliminar/<int:id_categoria>/", views.categorias_eliminar, name="categorias_eliminar"),
	path("categorias_formulario_editar/<int:id_categoria>/", views.categorias_formulario_editar, name="categorias_formulario_editar"),
	path("categorias_actualizar/", views.categorias_actualizar, name="categorias_actualizar"),

	path("productos/", views.productos, name="productos"),
	path("productos_formulario/", views.productos_formulario, name="productos_formulario"),
	path("productos_guardar/", views.productos_guardar, name="productos_guardar"),
	path("productos_eliminar/<int:id_producto>/", views.productos_eliminar, name="productos_eliminar"),
	path("productos_formulario_editar/<int:id_producto>/", views.productos_formulario_editar, name="productos_formulario_editar"),
	path("productos_actualizar/", views.productos_actualizar, name="productos_actualizar"),

	path("buscar/", views.buscar, name="buscar"),
	path("ver_perfil/", views.ver_perfil, name="ver_perfil"),
	path("formulario_cambiar_clave/", views.formulario_cambiar_clave, name="formulario_cambiar_clave"),
	path("cambiar_clave/", views.cambiar_clave, name="cambiar_clave"),

	# carrito de compra
	path("carrito_add/", views.carrito_add, name="carrito_add"),
	path("ver_carrito/", views.ver_carrito, name="ver_carrito"),
	path("eliminar_item_carrito/<int:id_producto>/", views.eliminar_item_carrito, name="eliminar_item_carrito"),
	path("vaciar_carrito/", views.vaciar_carrito, name="vaciar_carrito"),

	path("guardar_compra/", views.guardar_compra, name="guardar_compra"),
	# path("enviar_correo/", views.enviar_correo, name="enviar_correo"),
]

"""
# Si existen usuario previos y deseamos generar token
from rest_framework.authtoken.models import Token

from .models import Usuario
for user in Usuario.objects.all():
    Token.objects.get_or_create(user=user)
"""