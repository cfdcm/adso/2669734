from django.contrib import admin
from .models import *
from django.utils.html import mark_safe

# Register your models here.


class CategoriaAdmin(admin.ModelAdmin):
	list_display = ["id", "nombre_cat", "desc"]
	search_fields = ["nombre_cat", "desc"]


class ProductoAdmin(admin.ModelAdmin):
	list_display = ['id', 'nombre', 'stock', 'precio', 'subtotal', 'calc_subtotal', 'categorias', 'id_cat', 'ver_foto']
	search_fields = ["nombre", "categorias__nombre_cat", "categorias__desc"]
	list_filter = ["categorias"]
	# list_editable = ["nombre", "categorias"]

	def calc_subtotal(self, obj):
		return mark_safe(f"<strong style='color:red;'>${obj.stock * obj.precio}</strong>")

	def ver_foto(self, obj):
		return mark_safe(f"<img src='{obj.foto.url}' width='30%' />")

	def id_cat(self, obj):
		return obj.categorias.id

class UsuarioAdmin(admin.ModelAdmin):
	list_display = ['nombre', 'apellido', 'nick', 'rol', 'foto', 'email', 'token', 'ver_foto']
	# list_editable = ['correo']
	def ver_foto(self, obj):
		return mark_safe(f"<img src='{obj.foto.url}' width='20%' />")

admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Usuario, UsuarioAdmin)


@admin.register(Compra)
class CompraAdmin(admin.ModelAdmin):
	list_display = ['id', 'fecha', 'usuario', 'estado']


@admin.register(DetalleCompra)
class DetalleCompraAdmin(admin.ModelAdmin):
	list_display = ['id', 'id_compra', 'producto', 'cantidad', 'precio']


