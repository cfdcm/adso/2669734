from .models import *
from rest_framework import serializers


class CategoriaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Categoria
		# fields = ['id', 'titulo', 'imagen', 'estreno', 'resumen']
		fields = '__all__'


class ProductoSerializer(serializers.ModelSerializer):
    # Campos con relaciones, para traer dato específico
	categoria_name = serializers.StringRelatedField(many=False, source='categorias.nombre_cat')
    
	class Meta:
		model = Producto
		fields = ['id', 'nombre', 'stock', 'precio', 'categorias', 'categoria_name', 'foto']
		# fields = '__all__'


class UsuarioSerializer(serializers.ModelSerializer):
	class Meta:
		model = Usuario
		fields = ['id', 'nombre', 'apellido', 'nick', 'rol', 'foto']


class LoginSerializer(serializers.ModelSerializer):
	class Meta:
		model = Usuario
		fields = ['id', 'nick', 'rol']

