from django.contrib.auth.models import AbstractUser

from .authentication import CustomUserManager

from django.db import models

# Create your models here.
# ORM : Database Object relational Mapping

class Categoria(models.Model):
	nombre_cat = models.CharField(max_length=100)
	desc = models.TextField()

	def __str__(self):
		return f"{self.nombre_cat}"


class Producto(models.Model):
	nombre = models.CharField(max_length=200)
	stock = models.IntegerField()
	precio = models.IntegerField()
	categorias = models.ForeignKey(Categoria, on_delete=models.DO_NOTHING)
	foto = models.ImageField(upload_to="fotos_productos/", default="fotos_productos/default.png")

	def subtotal(self):
		return f"${self.stock * self.precio}"

	def __str__(self):
		return f"{self.nombre}"

# Custom Authentication
class Usuario(AbstractUser):
	username = None
	nombre = models.CharField(max_length=254)
	apellido = models.CharField(max_length=254)
	nick = models.CharField(max_length=100, unique=True)
	password = models.CharField(max_length=254)
	# enum, set
	ROLES = (
		("ADMIN", "Administrador"),
		("SECRE", "Secretaria"),
		("VENDE", "Vendedor"),
		("USUAR", "Usuario"),
	)
	rol = models.CharField(max_length=5, choices=ROLES, default="USUAR", null=True, blank=True)
	# Pillow
	foto = models.ImageField(upload_to="fotos/", default="fotos/default.png")
	email = models.EmailField(max_length=254, default="jagarciac@sena.edu.co", unique=True)
	token = models.CharField(max_length=10, null=True, blank=True)
	objects = CustomUserManager()

	USERNAME_FIELD = 'nick'
	REQUIRED_FIELDS = ['nombre', 'apellido', 'email']

	def __str__(self):
		return f"{self.nombre} - {self.rol}"


class Compra(models.Model):
	fecha = models.DateTimeField(auto_now_add=True)
	usuario = models.ForeignKey(Usuario, on_delete=models.DO_NOTHING)
	ESTADO = (
		(1, "Creado"),
		(2, "Enviado"),
		(3, "Cancelado"),
	)
	estado = models.IntegerField(choices=ESTADO, default=1, blank=True)

	def __str__(self):
		return f"{self.id} - {self.usuario}"

class DetalleCompra(models.Model):
	id_compra = models.ForeignKey(Compra, on_delete=models.DO_NOTHING)
	producto = models.ForeignKey(Producto, on_delete=models.DO_NOTHING)
	cantidad = models.IntegerField()
	precio = models.IntegerField()

