# Ejercicio 1 de funciones avanzadas.

"""
Construir un juego que permita crear personajes, usando funciones.
El nombre_del_personaje debe ser obligatorio.
Además, tendrá la posibilidad de tener poderes, las opciones son:
	- Volar
	- Disparar
	- Super fuerza
	- Super salto
Cada uno de estos poderes serán funciones que deben crear.

Cuando se crea un personaje se debe imprimir su nombre_del_personaje y ejecutar
las funciones según los poderes seleccionados.

Crear un menú para que el usuario final, decida los poderes que quiere tener
su personaje, 0, 1 o todos.
"""

def volar():
	print("Estoy volando por la galaxia...!!")

def disparar():
	print("Estoy Disparando láser...!!")

def super_fuerza():
	print("Soy el más fuerte...!!")

def super_salto():
	print("Estoy Saltando muy alto...!!")


def crear_personaje(nombre, poderes):
	print(f"Personaje creado <<{nombre}>>\n")
	print(poderes)
	for p in poderes:
		eval(p+"()")



if __name__ == "__main__":
	nombre = input("Digite el nombre del personaje: ")
	poderes = []
	while True:
		poder = int(input(f""" Seleccione un poder para <<{nombre}>>: 
		1. - Volar
		2. - Disparar
		3. - Super fuerza
		4. - Super salto
		
		5. Terminar
		:"""))

		if poder == 1:
			poderes.append("volar")
		elif poder == 2:
			poderes.append("disparar")
		elif poder == 3:
			poderes.append("super_fuerza")
		elif poder == 4:
			poderes.append("super_salto")
		elif poder == 5:
			break
		else:
			print("ERROR: Intente de nuevo..")

	crear_personaje(nombre, poderes)

