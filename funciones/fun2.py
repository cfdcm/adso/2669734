# Funciones con parámetros o argumentos.
			# int, float, str, bool, colecciones

# definir la función
def saludar(nombre, apellido):
	print(f"Hola {nombre} {apellido}")


# ejecutando la función

saludar("pérez", 30)
saludar("cano", 15)
saludar()
