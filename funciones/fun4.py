# Funciones con parámetros arbitrarios. Argumentos

def sumar(*args):
	acum = 0
	for s in args:
		acum += s

	return acum


print(sumar(5))
print(sumar(1, 2))
print(sumar(1, 2, 8))
print(sumar(1, 2, 8, 10, 32, 40))
print(sumar(10, 32, 40))


