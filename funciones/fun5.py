# Funciones con parámetros por defecto

def saludar(persona="Sena"):
	return f"Hola {persona}"


pal = input("Digita cualquier cosa: ")
print(saludar(pal))

pal = input("Digita cualquier cosa: ")
print(saludar(pal))

print(saludar())
