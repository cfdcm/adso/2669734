# funciones con diccionarios arbitrarios
def caminar(**kwargs):
	for i in kwargs.items():
		print(f"{i[0]} - {i[1]}")


# Unpacking dict
# Unpacking tuple

clave = input("Digite una clave: ")
valor = input("Digite su valor: ")

persona = {clave: valor, "apellido" : "García", "edad" : 62}

caminar(**persona)


