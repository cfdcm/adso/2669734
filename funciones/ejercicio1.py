"""
Construir un programa usando funciones. Que permita convertir
de grados centígrados a fahrenheit y viceversa.
Usar un menú:
1. Convertir de °C a °F
2. Convertir de °F a °C
3. Salir
"""

def convertir_c_f(valor):
	return (valor * 9/5) + 32

def convertir_f_c(valor):
	return (valor - 32) * 5/9


while True:
	op = int(input("""Qué desea hacer?: 
					1. Convertir de °C a °F
					2. Convertir de °F a °C
					3. Salir
	"""))
	if op == 1:
		c = float(input("Digite los °C:"))
		print(f"Resultado: {convertir_c_f(c):.1f}")
	elif op == 2:
		f = float(input("Digite los °F:"))
		print(f"Resultado: {convertir_f_c(f):.1f}")
	elif op == 3:
		print("Bye!")
		break
	else:
		print("Error")
