from motor_fisicas import saltar, caminar, brincar as bmf
from sonidos import brincar

def saludar(n):
	print(f"Hola {n}")

if __name__ == "__main__":
	player = "ADSO"

	saltar()			# motor físicas
	caminar()			# motor físicas

	bmf()				# motor físicas
	brincar()			# sonidos

	saludar("Jorge")	# main
