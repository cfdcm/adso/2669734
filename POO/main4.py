class Carro:

	def __init__(self):
		self.__velocidad = 5

	def get_velocidad(self):
		return self.__velocidad

	def acelera(self, mas):
		if mas > 0:
			self.__velocidad += mas
			return True
		else:
			return False

	def frena(self, menos):
		if menos <= self.__velocidad and menos > 0:
			self.__velocidad -= menos
			return True
		else:
			return False


if __name__ == "__main__":
	listado = []
	for i in range(5):
		listado.append(Carro())

	while True:
		menu_carros = ""
		for i in range(1, len(listado)+1):
			menu_carros += f" - {i}"

		cual = int(input(f"Con cuál carro quiere trabajar?: {menu_carros[3:]}\n:")) - 1

		nueva = int(input("Digite la velocidad a aumentar: "))


		if listado[cual].acelera(nueva):
			print("Velocidad aumentada correctamente!!")
			print(f"Velocidad actual: {listado[cual].get_velocidad()}")
		else:
			print("Error: Velocidad no válida")
			print(f"Velocidad actual: {listado[cual].get_velocidad()}")

		nueva = int(input("Digite la velocidad a disminuir: "))

		if listado[cual].frena(nueva):
			print("Velocidad disminuida correctamente!!")
			print(f"Velocidad actual: {listado[cual].get_velocidad()}")
		else:
			print("Error: Velocidad no válida")
			print(f"Velocidad actual: {listado[cual].get_velocidad()}")

		print("="*60)

		contador = 1
		for c in listado:
			print(f"El Carro {contador} tiene velocidad: {c.get_velocidad()}")
			contador += 1

