class Carro:

	def __init__(self):
		self.__velocidad = 5

	def get_velocidad(self):
		return self.__velocidad

	def acelera(self, mas):
		if mas > 0:
			self.__velocidad += mas
			return True
		else:
			return False

	def frena(self, menos):
		if menos <= self.__velocidad and menos > 0:
			self.__velocidad -= menos
			return True
		else:
			return False


if __name__ == "__main__":
	w = Carro()

	nueva = int(input("Digite la velocidad a aumentar: "))

	if w.acelera(nueva):
		print("Velocidad aumentada correctamente!!")
		print(f"Velocidad actual: {w.get_velocidad()}")
	else:
		print("Error: Velocidad no válida")
		print(f"Velocidad actual: {w.get_velocidad()}")

	nueva = int(input("Digite la velocidad a disminuir: "))

	if w.frena(nueva):
		print("Velocidad disminuida correctamente!!")
		print(f"Velocidad actual: {w.get_velocidad()}")
	else:
		print("Error: Velocidad no válida")
		print(f"Velocidad actual: {w.get_velocidad()}")