class Cerveza:
	def __init__(self, *args):
		"""
		Sin argumentos, cerveza por defecto.
		1 parámetro: Presentación
		2 parámetros: Presentación y Marca, respectivamente.
		"""
		self.temperatura = 2
		self.estado = "Tapada"
		self.precio = 2800
		self.presentacion = "Botella"  # Lata - Plástico - Litrón
		self.marca = "Costeñita"

		if len(args) == 1:
			self.presentacion = args[0]
		elif len(args) == 2:
			self.presentacion = args[0]
			self.marca = args[1]


	def destapar(self):
		pass

	def enfriar(self):
		pass

	def regalar(self):
		pass


if __name__ == "__main__":
	# polimorfismo
	c1 = Cerveza()
	c2 = Cerveza("Lata")
	c3 = Cerveza("Botella", "Budweiser")
	c4 = Cerveza("Litrón", "Águila")

	print(f"""
	Cerveza1 ({c1.marca}): {c1.presentacion} 
	Cerveza2 ({c2.marca}): {c2.presentacion} 
	Cerveza3 ({c3.marca}): {c3.presentacion} 
	Cerveza4 ({c4.marca}): {c4.presentacion} 
	""")