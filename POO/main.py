class Usuario:
	# atributos de clase

	# atributos de instancia
	def __init__(self, nombre, nick, passwd):
		self.nombre = nombre
		self.nick = nick
		self.passwd = passwd
		self.activo = False

	def cambiar_contrasena(self, nueva1, nueva2):
		if nueva1 == nueva2:
			self.passwd = nueva1
			print("Contraseña cambiada correctamente!!")
		else:
			print("No coinciden las constraseñas...")

	def darme_de_baja(self):
		print("Ok, chao!")

	def __str__(self):
		return f"Nombre: {self.nombre} - Nick: {self.nick}"

if __name__ == "__main__":
	# crear objetos
	n = input("Digite su nombre: ")
	nn = input("Digite su nick: ")
	p = input("Digite su password: ")

	obj1 = Usuario(n, nn, p)

	print("*"*30)
	print(obj1)
	print("*"*30)

	print(obj1.nombre)
	print(obj1.nick)
	print(obj1.passwd)
	n1 = input("Digite su contraseña: ")
	n2 = input("Confirme su contraseña: ")

	obj1.cambiar_contrasena(n1, n2)
	print(obj1.passwd)

	obj1.darme_de_baja()
