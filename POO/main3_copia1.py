class Manzana:
    # atributo de clase
    conteo = 0

    def __init__(self, estrellas, tamano):
        # atributos de instancia
        self.calidad = estrellas
        self.tamano = tamano
        self.vendida = False
        Manzana.conteo += 1
        self.id = Manzana.conteo

    def vender(self):
        self.vendida = True
        print(f"Manzana {self.id} vendida!!")

    def __str__(self):
        return f"{self.id}"

if __name__ == "__main__":
    listado = []

    for m in range(1, 4):
        calidad = int(input("Digite la calidad de la manzana (1-5): "))
        tamano = input("Digite el tamaño de la manzana (Pequeña, Mediana, Grande): ").upper()
        listado.append(Manzana(calidad, tamano))

    print("Cuál manzana quiere vender?:")
    for m in listado:
        print(m)

    op = int(input(":"))

    listado[op-1].vender()

    print(f"""
    Calidad: {listado[op-1].calidad}
    Tamaño: {listado[op-1].tamano}
    Vendida: {listado[op-1].vendida}
    ID: {listado[op-1].id}
    """)

    # del listado[op-1]
    # listado.pop(op-1)
    # len(listado)
