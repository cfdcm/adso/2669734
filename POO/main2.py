class Carro:
	def __init__(self):
		self.__velocidad = 5

	def get_velocidad(self):
		return self.__velocidad

	def acelera(self, mas):
		if mas > 0:
			self.__velocidad += mas
			return True
		else:
			return False


if __name__ == "__main__":
	w = Carro()

	while True:
		try:
			nueva = int(input("Digite la velocidad a aumentar: "))
			if nueva > 0:
				break
			else:
				raise Exception("Valor no permitido.")
		except Exception as e:
			print(f"Dato no válido, digite un entero... {e}")

	if w.acelera(nueva):
		print("Velocidad aumentada correctamente!!")
		print(f"Velocidad actual: {w.get_velocidad()}")
	else:
		print("Error: Velocidad no valida")
		print(f"Velocidad actual: {w.get_velocidad()}")

