class Temperatura:

    def celsius_to_farenheit(self, grados_c):
        r = 1.8 * grados_c + 32
        return r

    def farenheit_to_celsius(self, grados_f):
        r = (grados_f - 32) / 1.8
        return r


if __name__ == "__main__":
    ejemplo1 = Temperatura()
    r = ejemplo1.celsius_to_farenheit(30)
    print(f"ºF = {r:.1f}")
    print("")
    r = ejemplo1.farenheit_to_celsius(r)
    print(f"ºC = {r:.1f}")
